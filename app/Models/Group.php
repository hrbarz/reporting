<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\GroupPresenter;

class Group extends Model
{
    use GroupPresenter;

    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['id', 'name','status','created_at','updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


}
