<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\ReportPresenter;
use Carbon;

class Report extends Model
{
    use ReportPresenter;

    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'reports';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['id', 'title','description','resumen','content','status','type','filereport','config','permissions','created_at','updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    protected $casts = [
        'config' => 'array',
        'permissions' => 'array',
        'content' => 'array',
    ];


}
