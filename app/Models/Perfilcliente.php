<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Perfilcliente extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'perfilcliente';
    protected $primaryKey = 'IDPerfilcliente';

    const CREATED_AT = 'Fechaalta';

    const UPDATED_AT = 'Fechamodificacion';

    protected $fillable = [
        'IDPerfilcliente', 'Nombre', 'Logo', 'Fechamodificacion', 'Fechaalta'
    ];

    public function modules()
    {
        return $this->belongsToMany(Module::class, 'perfilcliente_modules', 'perfilcliente_id', 'module_type');
    }
}
