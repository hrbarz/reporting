<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\MediaPresenter;

class Media extends Model
{
    use MediaPresenter;

    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'relation_id', 'relation', 'format', 'path', 'caption', 'position', 'created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}
