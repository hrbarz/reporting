<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Presenters\UserPresenter;

class User extends Authenticatable
{
    use UserPresenter;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 
                           'email', 
                           'token', 
                           'password',
                           'first_name', 
                           'last_name', 
                           'slug', 
                           'type', 
                           'status',
                           'created_at', 
                           'updated_at'
                        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /**
     * Generate a autologin url with a temporary Token
     */
    public function tokenUrl( $url, $utm=false )
    {
        $redirect = urlencode( $url . ($utm ? '?utm_medium=Email&utm_campaign='.$utm : '') );
        return route('token', ['token' => $this->token, 'redirect' => $redirect]);
    }   

}
