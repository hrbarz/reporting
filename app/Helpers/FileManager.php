<?php

namespace App\Helpers;

use Validator;
use Storage;
use Image;
use Str;

class FileManager {


    public function upload( $file )
    {
        if( !isset($file) ){ return false; }

        $path = $this->getPath( $file );

        $content = file_get_contents( $file->getRealPath() );

        if( Storage::put( $path , $content ) )
        {
            return $path;
        }

        return false;
    }

	public function download( $file )
	{
		if( !isset($file) ){ return false; }

		$path = $this->getPath( $file );

        $content = file_get_contents( $file );

        if( Storage::put( $path , $content ) )
        {
            return $path;
        }

		return false;
	}

	public function createFolder( $file )
	{
		// folder path /upload/2015-07/
		$folder = date('Y-m');

        // checking folder
        if( Storage::exists( $folder ) )
        {
            return $folder;
        }

        // creating folder
        Storage::makeDirectory( $folder );

        // creating thumbs folder
        Storage::makeDirectory( $folder.'/th' );

        return $folder;

	}

	public function getPath( $file )
	{

		// create upload folder if not exists;
        $folder = $this->createFolder( $file );

        // rename file / check is file uploaded        
        if(is_object($file))
        {
            $name = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();

        }
        else
        {
            $name = getFileName($file);
            $extension = getFileExtension($file);
        }

        $name = explode('.', $name);
        $file_name = Str::slug( $name[0] );

        // if name is chinese use the original name
        if( empty( $file_name ) )
        {
            $file_name = str_replace('　','',$name[0]);
        }

        return $folder . '/' . $file_name . '-' . rand(1000,9999) . '.' . $extension;

	}

	public function delete( $path )
	{
		Storage::delete( $path );
	}

	public function rename( $path, $name )
	{	
		$path_new = explode('/', $path);
		$path_new = str_replace( $path_new[ count($path_new)-1 ], $name, $path );
		Storage::move($path, $path_new);
	}

	public function validate( $file )
	{

		$validator = Validator::make( ['file' => $file] , [
            'file' => 'required|mimes:jpeg,bmp,png,gif,jpg,pdf'
        ]);

        if ($validator->fails()) 
        {
        	return false;
        }

        return true;
	}

	public function format( $path )
    {

    	$path = explode('.', $path);
    	$extension = $path[ count($path)-1 ];

        switch( Str::lower($extension) )
        {
            case 'jpg':
            case 'jpeg':
            case 'gif':
            case 'png':
            case 'bmp':

                return 'photo';

            default:

                return 'file';
        }

        return 'file';

    }

}

?>