<?php

	function is_route( $name )
	{
		if( Route::currentRouteName() == $name)
		{
			return true;
		}

		return false;
	}

	function is_route_path( $name )
	{
		/*if( strpos(Route::currentRouteName(), $name) !== false )
		{
			return true;
		}*/

        if(preg_match('/('. $name .')/i' , Route::currentRouteName() , $m) === 1)
        {
            return true;
        }

		return false;
	}

	function current_route()
	{
		return Route::currentRouteName();
	}

    function route_path()
    {
         $route = explode('.', current_route());

         return $route[0];
    }

	function is_category( $category, $slug )
	{
			if( !$category ){ return false; }

			if( $category->slug == $slug  || ( $category->parent_id && $category->parent->slug == $slug ) )
			{
				return true;
			}

			return false;
	}

	function array_to_object($array)
	{
    	return (object) $array;
	}

	function object_to_array($object)
	{
    	return (array) $object;
	}

	function selectBox( $data, $blank=true )
    {

	    if(!$data){ return []; }

		if( is_object($data) )
		{
			return modelSelectBox($data, $blank);
		}

		if( is_array($data) )
		{
			return arraySelectBox($data, $blank);
		}

		return [];

    }

	function modelSelectBox( $model, $blank=true )
	{
		if( $blank === true )
		{ 
			$options[''] = '';
		}

		if(!is_array($model))
		{
			if( !$model || !$model->count() ){ return []; }
		}

		foreach( $model as $option )
		{
                if( $option->getTable() == 'geo' )
                {
                     $options[ $option->id ] = $option->trans('name');
                }
                else
                {
				    $options[ $option->id ] = ( isset($option->name) ? $option->name : ( isset($option->title) ? $option->title : $option->code ) );
                }
		}

		return $options;
	}

	function arraySelectBox( $array, $blank=true )
	{
		if( $blank  === true)
		{
			$options[''] = '';
		}

		if( !count($array) ){ return []; }

		foreach( $array as $option )
		{

			if( is_object($option) )
			{
				return modelSelectBox($array, $blank);
			}

			if( $option )
			{
				$options[ $option ] = ucwords( str_replace(':',' ', $option) );
			}
		}

		return $options;
	}


	function force_redirect($url, $permanent = false)
	{
		if($permanent)
		{
			header('HTTP/1.1 301 Moved Permanently');
		}

		header('Location: '.$url);

		exit();
	}

	function random_color_part() 
	{
    	return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}

	function random_color()
	{
	    return random_color_part() . random_color_part() . random_color_part();
	}


	function reports_header($header)
	{
		if(strpos($header, 'percentage') !== false)
        {
        	$return = str_replace('percentage','',$header);
        }
        elseif( $header == 'id'){
        	$return = '#';
        }
        else
        {
        	$return = $header;
        }

        return $return;
	}

	/*function reports_value($header,$value, $header, $r)
	{
        if(strpos($header, 'percentage' ) !== false)
        {
        	return '<div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100" style="width: '.$value.'%;">
                      '.$value.'%
                    </div>
                  </div>';
        }

        
        /*
        if( substr($header,0,1) == '_' )
        {
            return 'http://planetyze.com/admin/guidebook/'. $value .'/en/edit';
        }
        
        

         return $r->{$header};
	}*/

	function seo( $key, $object=false )
	{

		return App\Helpers\SEO::generate($key, $object);

	}

	function check_reply( $reply)
	{ 
		if($reply!='')
		{
			return 'replied';
		}
		else
		{
			return 'pending';
		}
	}

	function lang_trans($lang)
	{
		return $lang !='' ? strtolower ($lang): config('app.fallback_locale') ;
	}

	function token()
	{
		return Hash::make(csrf_token());
	}

	function is_campaign( $utm )
	{
		
		if( isset($_GET['utm_campaign']) )
		{
			if( $_GET['utm_campaign'] == $utm )
			{
				return true;
			}
		}

		return false;
	}

	function durations($from = 0, $to = 23, $minutes = false, $am_pm = true)
    {

        $timetable = array();

        for ($i = $from; $i <= $to; $i ++)
        {


            if ($i >= 0 and $i <= 12)
            {

                if (strlen($i) == 1)
                {
                    $key = '0' . $i . ':00';
                } else
                {
                    $key = $i . ':00';
                }

                $time = $key . ' AM';

                if ($i == 12)
                {
                    $time = $key . ' PM';
                }

            }


            if ($i >= 13 and $i <= 24)
            {

                $key = $i . ':00';

                if ($i == 24)
                {
                    $key = '00:00';
                }

                $time = $key . ' PM';
            }


            if (!$am_pm)
            {
                $time = $key;
            }

            $timetable[ $key ] = $time;

            if ($minutes)
            {
                for ($x = 1; $x < round(60 / $minutes); $x ++)
                {
                    $minute = $x * $minutes;

                    if (strlen($minute) == 1)
                    {
                        $minute = '0' . $minute;
                    }

                    $time = str_replace(':00', ':' . $minute, $time);
                    $key = str_replace(':00', ':' . $minute, $key);

                    $timetable[ $key ] = $time;

                }
            }

        }

        return $timetable;

    }


    function get_list_days_month()
    {	
    	$list = ['01','02','03','04','05','06','07','08','09','10',
    			'11','12','13','14','15','16','17','18','19','20',
    			'21','22','23','24','25','26','27','28','29','30',
    			'31'];

    	return $list;
    }

    function get_list_month_year()
    {	
    	$list = [
    		(object)['id' => '01' , 'name' => 'January'],
    		(object)['id' => '02' , 'name' => 'February'],
    		(object)['id' => '03' , 'name' => 'March'],
    		(object)['id' => '04' , 'name' => 'April'],
    		(object)['id' => '05' , 'name' => 'May'],
    		(object)['id' => '06' , 'name' => 'June'],
    		(object)['id' => '07' , 'name' => 'July'],
    		(object)['id' => '08' , 'name' => 'August'],
    		(object)['id' => '09' , 'name' => 'September'],
    		(object)['id' => '10' , 'name' => 'October'],
    		(object)['id' => '11' , 'name' => 'November'],
    		(object)['id' => '12' , 'name' => 'December']
    	];

    	return $list;
    }

    function get_list_years()
    {
    	$list = [];

    	$start = date('Y') - 10;
    	$end = (date('Y') - 99);

    	for ($i = $start ; $i > $end ; $i--)
    	{ 
    		$list[] = $i;
    	}

    	return $list;
    }

    function is_provider()
    {
    	 $user = Auth::user();
    
    	 if($user->type == 'guide')
    	 {
    	 	return true;
    	 }

    	 return false;
    }

    function content( $text )
    {
    	return nl2br( $text );
    }

    function requestClear( $request )
    {
    	$params = [];

    	foreach( $request as $key=>$value)
    	{
    		if( $key == 'geo' ){ continue; }

    		if( !$value || $value == '0' ){ continue; }

    		$params[] = $key .'='.$value;

    	}
    	
    	if( $params )
    	{
    		return '?'. implode('&', $params);
    	}

    	return  '';
    }

    function jsArray( $array, $tag="'" )
    {
    	if( !$tag ){ $tag = ''; }

    	$array = implode(',', $array);
    	$array = str_replace(' ', '', $array);
    	$array = str_replace(',', $tag.",".$tag, $array);
    	return $tag. $array .$tag;
    }

    function getTime( $time )
    {
        return date("g:i A", strtotime($time));

        if (strlen($time) == 8)
        {
            $time = substr($time, 0, - 3);
        }

        $hour = substr($time, 0, 2);

        if ($hour >= 0 && $hour <= 11)
        {
            return $time . ' AM';
        }

        return $time . ' PM';
    
    }

    function get_domain_url($url)
    {
        $parse = parse_url($url);

        return isset($parse['host']) ? $parse['host'] : $url;
    }


    function has_days( $value )
    {

        if( !isset($value) || !is_array($value) )
        {
            return false;
        }

        foreach( $value as $day )
        {
            if( isset($day['status']) )
            {
                return true;
            }
        }

        return false;
    }

    function get_photos_id($content)
    {
        $pattern = "/\[photo=(.*)\]/smU";
        
        preg_match_all($pattern,$content,$m,PREG_SET_ORDER);

        $photos_id = [];

        if(count($m) > 0)
        {
            foreach($m as $key => $value)
            {
                $photos_id[] = ['photo_id' => $value[1] , 'block' => $value[0]];
            }
            
        }

        return $photos_id ;

    }

    function check_if_exists_days_active($schelude)
    {
        $exists = false;

        if(count($schelude) > 0)
        {
            foreach ($schelude as $key => $value) {
                
                if($value['to'] != '' || $value['from'] != '' )
                {
                    $exists =  true;
                }
            }
        }

        return $exists;
    }

    function diffDays( $date, $now=false )
    {
        if(!$now)
        {
            $now = date('Y-m-d');
        }

        $dStart = new DateTime( $now );
        $dEnd  = new DateTime( $date );
        $dDiff = $dStart->diff($dEnd);
        $dDiff->format('%R');
        return $dDiff->days;
    }

    function dateFormat( $date, $format = 'Y-m-d' )
    {
        return date($format, strtotime( $date ));
    }

    function slug($text)
    {
        $slug = Str::slug($text);

        if($slug == '')
        {
            $slug = str_replace(' ', '', $text);            
        }

        return $slug;

    }

    function currencyConverter( $price, $currency )
    {
        $finance = new \App\Helpers\CurrencyConverter();
        $r = $finance->calculator( $price, $currency, session('currency_id') );

        return $r;
    }

    function youtube( $url, $width='100%', $height=false )
    {
        return \App\Helpers\Youtube::embed( $url, $width, $height );
    }

    function youtube_info( $url )
    {
        return \App\Helpers\Youtube::info($url);
    }

    function array_clean( $array )
    {
        if( !$array )
        {
            return false;
        }

        $new_array = [];

        foreach($array as $key=>$value)
        {
            if( $value != '' )
            {
                $new_array[ $key ] = ( is_array($value) ? array_clean($value) : $value );
            }
        }

        return $new_array;
    }

    function getPhotos( $text )
    {
        $list = [];

        preg_match_all('/(src|SRC)=["\']?([^"\'>]+)["\']?/', $text, $match);
        
        if($match[2])
        {
            foreach ($match[2] as $key => $value)
            {               
                $list[] = $value;
            }
        }

        return $list;
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) 
    {
      if( !$lat1 || !$lon1 || !$lat2 || !$lon2 ){ return false; }

      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);

      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
            return $miles;
          }
    }

    function phone( $phone )
    {
        if( !$phone ){ return false; }

        $phone = implode('-', $phone);

        if( strlen($phone) > 4  )
        {
           return $phone;
        }

        return false;
    }
    
    function filter_security($text)
    {
        $filter = new App\Helpers\Filters\Filters();

        return $filter->run($text);
    }

    function links($text)
    {
        $links = new App\Helpers\Links\Links();
            
        return $links->run($text);
        
    }

    function checkIn( $days = 1, $check_in=false )
    {

         $check_in = ( $check_in ? $check_in : date('Y-m-d') );
         $check_in = Carbon::parse( $check_in );
         $check_in = $check_in->addDays( $days )->format('Y-m-d');

         return $check_in;
    }

    function checkOut( $days = 2, $check_out=false )
    {
        $check_out = ( $check_out ? $check_out : date('Y-m-d') );
        $check_out = Carbon::parse( $check_out );
        $check_out = $check_out->addDays( $days )->format('Y-m-d');

        return $check_out;
    }


    function get_headers_from_curl_response($response)
    {
        $headers = array();

        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

        foreach (explode("\r\n", $header_text) as $i => $line)
            if ($i === 0)
                $headers['http_code'] = $line;
            else
            {
                list ($key, $value) = explode(': ', $line);

                $headers[$key] = $value;
            }

        return $headers;
    }

    function datatable( $html )
    {

        $tag_open  = '--- datatable ---';
        $tag_close = '--- /datatable ---';

        if( !$html ){ return $html; }

        $original = getInsideTag( $tag_open, $tag_close, $html );

        if( !$original ){ return $html; }

        $content = strip_tags($original);
        $content = explode('[', $content);

        $table = [];

        foreach($content as $line)
        {
            $key = getInsideTag('[', ']', '['.$line);
            $value = str_replace('['.$key.']', '', '['.$line);

            if( strlen($key) > 2 && $value )
            {
                $value = str_replace('#br#', '<br />', $value);
                $table[ $key ] = $value;
            }
        }

        $table = datatableDraw( $table );

        return str_replace( $tag_open . $original . $tag_close, $table, $html );

    }

    function datatableDraw( $table )
    {
        $html = '<table class="table table-data table-default table-data-v2" style="margin-top:20px; margin-bottom:20px"><tbody>';

        foreach($table as $key => $value)
        {
            $html .= '<tr><td>'. $key .'</td><td>'. $value .'</td></tr>';
        }

        $html .= '</tbody></table>';

        return $html;
    }
    
    function getInsideTag( $tag_open, $tag_close, $html )
    {
        if( !Str::contains($html, $tag_open) || !Str::contains($html, $tag_close) )
        {
            return false;
        }

        $parts = explode($tag_open, $html);
        $parts = explode($tag_close, $parts[1]);

        return $parts[0];

    }

    function getPage( $slug )
    {
        return \App\Models\Page::where('slug', $slug)->first();
    }

    function decimal($num,$decimal=1)
    {
        if($num == 0) return $num;
        
        return number_format($num,$decimal);
    }

    function content_copyright( $text )
    {

        $text = trim($text);

        if( strpos($text, '(') === false )
        {
            return $text;
        }

        $copyright = explode('(', $text, 2);

        if( substr($copyright[1],-1) == ')' )
        {
            $copyright = substr($copyright[1],0,-1);
        }
        else
        {
            $copyright = explode(')', $copyright[1]);
            $copyright = $copyright[0];
        }

        if( strpos($copyright, '//') !== false )
        {
            $parse = parse_url($copyright);
            $copyright_domain = @$parse['host'];
            $copyright_domain = str_replace("www.", "", $copyright_domain);

            $copyright_text = "<a href='". $copyright ."' target='_blank'>". $copyright_domain . "</a>";
        }
        else
        {
            $copyright_text = $copyright;
        }

        $copyright_html  = " <span class='copyright'>image by (";
        $copyright_html .= $copyright_text;
        $copyright_html .= ")</span>";

        $text = str_replace('(' . $copyright . ')', $copyright_html, $text);
        return $text;

    }

    function getFileName($file)
    {
        $filename = parse_url($file, PHP_URL_PATH);

        return pathinfo($filename, PATHINFO_BASENAME);
    }

    function getFileExtension($file)
    {
        $filename = parse_url($file, PHP_URL_PATH);

        return pathinfo($filename, PATHINFO_EXTENSION);
    }

    function to_rate_planetyze($val,$base)
    {
        return decimal(($val * 5 )/10);
    }


    function parse_places_in_text($text)
    {
        $list = [ 
            '@' => [],
            '#' => []
        ];

        $regexp = "<place\s[^>]*data-id=(\"??)([^\" >]*?)\\1[^>]* data-relation=\"([^\"]*)\">(.*)<\/place>";
        preg_match_all("/$regexp/siU", $text, $matches, PREG_SET_ORDER);

        foreach ($matches as $key => $value)
        {
            $list['@'][$value[2]] = ['relation' => $value[3] , 'title' => $value[4], 'id' => $value[2] ];
        }

        $regexp = "\\[([^\"]*)=([^\"]*)\\]";
        preg_match_all("/$regexp/siU", $text, $matches2, PREG_SET_ORDER);

        foreach ($matches2 as $key => $value)
        {
            $list['#'][$value[2]] = ['relation' => $value[1] , 'slug' => $value[2]];
        }

        return $list;
    }

    function get_tpl_places($type,$data)
    {
        if($type == '@')
        {
            return '<place data-id="'.$data['id'].'" data-relation="'.$data['relation'].'">'.$data['title'].'</place>';
        }        

        if($type == '#')
        {
            return '['.$data['relation'].'='.$data['slug'].']';
        }

    }

    function get_link_places($data)
    {
        return ' <a href="'. $data['url'] .'">'. $data['title'] .'</a> ';
    }

    function timeIn($timezone = 'Asia/Tokyo')
    {
        $date = new DateTime('today', new DateTimeZone($timezone));
        return $date->format('U');        
    }

    function plataform_source()
    {
        return Session::get('source')=='web'?'web':'app';
    }


    function data_post_group_report($data)
    {
        $data_group = [];
        foreach ($data['group'] as $key => $value)
        {
           $data_group[$key]['name'] = $value;
           $data_group[$key]['type'] = config('reports.columns_all.'.$value);
           $data_group[$key]['from'] = $data['from'][$key];
           $data_group[$key]['to']   = $data['to'][$key];           
        }

        return $data_group;
    }

    function index_case($text)
    {
        return str_replace('-','_', str_slug($text));
    }

    function isNameException($name)
    {
        return in_array($name, config('reports.name_exceptions'));
    }
?>
