<?php

namespace App\Helpers\Search;

use App\Helpers\Search\SearchExport;
use App\Helpers\Search\SearchCollection;
use App\Helpers\Search\SearcFilter;
use Cache;

class Search {

  use SearchFilter;
  use SearchExport;
  use SearchCollection;

  public $repository;

  public function __construct( $repository )
  {
      $this->repository = $repository;
      $this->filters_hidden = $this->repository->filters_hidden;
  }

  public function run( $results_x_page=false, $cache=false )
  {

    

      $query = $this->repository->model->query();

      // adding filters to query
      if( count($this->filters) > 0 )
      {
          foreach( $this->filters as $key => $value )
          {
              if( $filter = $this->filterExists( $key ) )
              {
                  $query = $this->repository->$filter( $query, $value );
              }
          }
      }

      $query = $this->repository->globalFilter( $query );
      
      // has Cache
      
      if( $cache )
      {
          $cache_key = $this->getCacheKey( $query );

          if( $search = $this->hasCache( $cache_key ) )
          {
              return $this->results = $search;
          }
      }
      


      if( $results_x_page )
      {
          $this->results = $query->paginate( $results_x_page );
      }
      else
      {
        $this->results = $query->get();  
      }
      

      
      if( $cache )
      {
          $this->saveCache( $cache_key, $this->results );
      }
      

      return $this->results;

  }


  public function hasCache( $cache_key )
  {

      if( Cache::has( $cache_key ) )
      {
          return Cache::get( $cache_key );
      }

      return false;
  }

  public function saveCache( $cache_key, $search )
  {
      Cache::put($cache_key , $search, 720);

  }

  public function getCacheKey( $query )
  {
      return 'search.'. md5( $query->toSql().serialize($query->getBindings()) );
  }


}
