<?php

namespace App\Helpers\Search;

use App\Helpers\Search\SearchExport;
use DB;
use stdClass;

class ReportView {

  use SearchExport;
  use SearchCollection;

  public $repository;
  public $headers = [];
  public $reports = [];
  public $results_json_char = [];
  public $export_all = 'true';
  public $export_multiple = 'true';

  public function __construct( $repository, $report )
  {
      $this->repository = $repository;
      $this->report = $report;
  }

  public function run( )
  {
      $query_list = $this->processQuery($this->report->querytext);

      foreach ($query_list as $key => $value) {

          $results = DB::select($value['query']);

          $this->reports[$key] = (object)[
                'id'                => $key,
                'results'            => $results,
                'title'             => $value['title'],
                'result_json_char'  => $this->formatJsonChar($results),
                'header'            => isset($results[0]) ? array_keys(get_object_vars($results[0])) : [],
            ];
      }
      
  }

  public function processQuery($query)
  { 
      $query_list = [];

      if(strpos($query,'---') !== false)
      {
          $blocks = explode('---',$query);

          if( trim($blocks[0]) == '')
          {
              array_shift($blocks);
          }
          foreach ($blocks as $key => $value) {
          
            if(strpos($value,':') !== false)
            {
                $block  = explode(':',$value);

                $query_list[$key]['title'] = $block[0];
                $query_list[$key]['query'] = $block[1];
            }
          
          }
          
      }else
      {

        $query_list[0]['title'] = '';
        $query_list[0]['query'] = $query;

      }

        return $query_list;      

  }

  public function formatJsonChar($results)
  {
      $formated = [];

      if(count($results) > 0 && isset($results[0]->percentage) && isset($results[0]->name)) {

          foreach ($results as $key => $value) {
              
              $color = $this->get_color_chat($key); 

              $formated[$key] = (object)[ 
                                'value' => (int) $value->percentage,
                                'label' => $value->name,
                                'color' => $color['color'],
                                'highlight' => $color['highlight'],
                                ];

              
          }

      }
      return json_encode($formated);

  }

  public function get_color_chat($i)
  {
    $colors = [
      ['color' => '#F7464A', 'highlight'=>'#FF5A5E'],
      ['color' => '#46BFBD', 'highlight'=>'#5AD3D1'],
      ['color' => '#FDB45C', 'highlight'=>'#FFC870'],
    ];
    return $colors[$i];
  }

  public function isEmpty($result)
  {
      if(count($result) == 0)
        return true;

      return false;
  }


}
