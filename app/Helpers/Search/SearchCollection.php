<?php

namespace App\Helpers\Search;

use Request;

trait SearchCollection {

  public $results = [];

  public function results()
  {
    return $this->results;
  }

  public function url( $data = false )
  {

     parse_str( Request::server('QUERY_STRING'), $query );

     
     if( method_exists( $this, 'getFilters' ) )
      $query = array_merge($query, $this->getFilters());

     if( is_array($data) && is_array($query) )
     {
        $query = array_merge($query, $data);
     }

     $query = http_build_query( $query );

     if( !$query )
     {
        return '';
     }

     return '?' . $query;

  }

  public function pagination($filters=true)
  {
      if(method_exists($this->results, 'appends')) 
      { 
        $html  = '<div class="text-center">';
        $html .= $this->results->appends( $filters ? @$this->getFilters() : null);
        $html .= "</div>";

        return $html;
      }

      return '';
  }

  public function total()
  {

      if( method_exists( $this->results, 'total') )
      {
          $total = $this->results->total();

          if( $total )
          {
             return $total;  
          }
          
      }

      return $this->results->count();
  }


  public function ids()
  {
      return $this->results->pluck('id')->all();
  }

  public function isEmpty()
  {
      if( !$this->results ){ return true; }

      if( $this->results->count() )
      {
        return false;
      }

      return true;
  }

  public function attach( $collection )
  {

      if( count($collection) <= 0 )
      {
         return false;
      }

      foreach( $collection as $item )
      {
          $this->results->push( $item );
      }
  }

}
