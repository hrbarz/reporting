<?php

namespace App\Helpers\Search;

trait SearchFilter {

  public $filters = [];
  public $filters_hidden = [];


  public function setFilters( $data )
  {
    if( !$data ){ return false; }

    foreach($data as $key => $value)
    {
       $this->setFilter( $key, $value );
    }

    return $this->filters;

  }


  public function setFilter( $key, $value )
  {

     if( !($value) && $value !== 0 ){ return false; }

      if( $filter = $this->filterExists( $key ) )
      {
          $this->filters[ $key ] = $value;
          return true;
      }

      return false;
  }

  public function unsetFilter($key)
  {
      unset($this->filters[$key]);
  }

  public function filterExists( $key )
  {
      $filter = camel_case($key) . 'Filter';

      if( method_exists( $this->repository, $filter ) )
      {
          return $filter;
      }

      return false;
  }


  public function getFilters()
  {
      $query = $this->filters;

      if( !$this->filters_hidden ){ return $query; }

      foreach( $this->filters_hidden as $field )
      {
          unset( $query[$field] );
      }

      return $query;
  }

  public function filter( $key, $default=false )
  {
      return ( isset( $this->filters[ $key ] ) ? $this->filters[ $key ] : $default );
  }

  public function filterClass( $key, $value=false )
  {
      if( (!$this->filter($key) && $value === false ) || $this->filter($key) == $value )
      {
        return  'active';
      }

      return false;
  }


}
