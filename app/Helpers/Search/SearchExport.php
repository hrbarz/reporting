<?php

namespace App\Helpers\Search;

use App\Helpers\Export;
use App\Helpers\Helper;
use Excel;

trait SearchExport {

  public $export_file   = 'export_{model}_search.csv';
  public $export_path   = 'export/';

  public function export()
  {

      $model = get_class( $this->repository->model );

      $this->export_file = str_replace('{model}', Helper::className( $model ), $this->export_file);
      $this->export_path = public_path() . '/' . $this->export_path . $this->export_file;

      $this->exportMultiple();
     
      $data = $this->exportData();

      Export::CSV( $data, $this->export_path );

      $this->exportHeaders();

  }


  public function exportHeaders()
  {

      header('Content-Encoding: SJIS');
      header('Content-type: text/csv; charset=Shift_JIS');
      header('Content-Disposition: attachment; filename='. $this->export_file );
      echo file_get_contents($this->export_path);
      die();

  }


  public function export_link()
  {
      return $this->url(['export' => true]);
  }

  public function exportMultiple()
  {
      if(isset($this->export_multiple))
      {
          $name_file = str_replace('.csv','',$this->export_file );

          Excel::create( $name_file , function($excel) use ($name_file)
          {

              foreach ($this->reports as $key => $report)
              {

                  $title = $report->title!='' ? $report->title : $name_file;

                  $excel->sheet($title, function($sheet) use ($report) 
                  {
                  
                      $data = $this->exportData($report);
                      $sheet->fromArray($data, null, 'A1', false, false);
                  
                  });

              }

          })->export('xls');
          
      }
  }

  public function exportData($report=null)
  {
      if(is_null($report)){

          $report = $this;

      }

      $header = $this->getHeadersExport($report);

      $data[] = $header;

      foreach($report->results as $row)
      {
          $item = [];

          foreach($header as $field)
          {
             $item[] = $this->exportField( $row, $field );
          }

          $data[] = $item;
      }

      return $data;
  }

  public function getHeadersExport($report = null)
  {
      if(!is_null($report) && isset($this->export_all))
      {
          return $report->header;
      }else
      {
          return $this->repository->export;
      }

  }


  public function exportField( $row, $field )
  {
      $field_export = camel_case( $field ) . 'Export';

      if( method_exists( $row, $field_export ) )
      {
          return $row->$field_export();
      }

      return $row->$field;
  }

}
