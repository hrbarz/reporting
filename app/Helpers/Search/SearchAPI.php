<?php

namespace App\Helpers\Search;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

use App\Helpers\Search\SearchExport;
use App\Helpers\Search\SearchCollection;
use App\Helpers\Search\SearcFilter;

class SearchAPI {

  use SearchFilter;
  use SearchExport;
  use SearchCollection;

  public $repository;

  public $total;

  public function run($callback)
  {
      $data = $callback();

     // $this->results = collect( $data->result ?:[]);

      $this->total = @$data->total;

      if( !$data )
      {
         return $this->results = false;
      }


      $this->results = $this->paginator($data->result, count($data->result));
    //  dd($this);
  }

  public function paginator($result , $perPage = null, $pageName = 'page', $page = null)
  {
        if($perPage)
        {
          $total = $this->total;

          return new LengthAwarePaginator($result, $total, $perPage, $page, [
              'path' => Paginator::resolveCurrentPath(),
              'pageName' => $pageName,
          ]);
        }

        return null;
  }

  public function setFilter( $key, $value )
  {

    $this->filters[ $key ] = $value;
    
  }

}
