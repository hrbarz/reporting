<?php

namespace App\Helpers;

use Request;
use Str;

class Nav
{

	
	public static function all()
	{
		$data = array();
		
		if( !session()->has('nav') ){ return false; }

		$limit = 25;

		foreach( session('nav') as $key => $url )
		{
			if(!is_array($url))
			{

				$title = Str::title($key);

				if( strlen($title) > $limit )
				{
					$title = Str::limit($title,$limit);
				}

				$data[] = (object) ['name' => $title , 'url' => $url ];

			}

		}

		return $data;
	}

	public static function render()
	{
		
		if( !session()->has('nav') ){ return false; }

		$limit = 25;
		$html = '<ul class="breadcrumb">'. "\n";

		foreach( session('nav') as $key => $url )
		{
			if(!is_array($url))
			{
				$title = Str::title($key);

				if( strlen($title) > $limit )
				{
					$title = Str::limit($title,$limit);
				}

				$html .= '<li><a href="'. $url .'">'. $title .'</a></li>'. "\n";
			}

		}

		$html .= "</ul>". "\n";
		
		Nav::expire();

		return $html;

	}

	
	public static function expire()
	{ 
		session()->forget('nav');
	}

	public static function put( $key, $url )
	{
		session(['nav.'.$key => $url]);
	}

	
}

