<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\ReportRepository;
use App\Models\Report;
use Illuminate\Http\Request;
use Component\Report\ReportBuilder;
use Auth;
use Hash;
use Session;
use Input;

class ReportController extends Controller {

   public function __construct( ReportRepository $reportRepository , Request $request )
   {
        if($request->input('token-reports'))
        {
           $token_reports = $request->input('token-reports');

           Session::put('token_reports', $token_reports);
        }
        if($request->input('client-reports'))
        {
           $client_reports = $request->input('client-reports');

           Session::put('client_reports', $client_reports);
        }

        $this->reportRepository = $reportRepository;
   }

   function existTokenReport($textToken)
   {
      return true;
      $token_reports = Session::get('token_reports', null);

      if(Hash::check($textToken, $token_reports)) return true;
       return false;
   }

   function existPermissions($permissions)
   {
       $client_reports = Session::get('client_reports', null);

       if(count($permissions) > 0 )
       {
          foreach ($permissions as $key => $value)
          { 
            if(md5('report_3ce_'.$key) == $client_reports) return $key;
          }
       }

       return false;

   }

   public function view(Report $report, Request $request)
   {
      if($request->input('token-reports'))
      {
         return redirect()->route('report', $report->id);
      }

      if (!$this->existTokenReport('report_3ce_'.$report->id) || 
          !$this->existPermissions($report->permissions))
      {
         abort(404);
      }      

      $this->report = $report;

      $client = $this->existPermissions($report->permissions);

      $_get = ['medio'=>'','localidad'=>'','sexo'=>'','edad'=>'','nse'=>'','resultado' => 'basic-percent'];
      $_get = array_merge($_get,$request->all());

      $filters = [];
      if(@$_get['localidad']!='')
      {
        $filters['localidad'] = $_get['localidad'];
      }
      if(@$_get['sexo']!='')
      {
        $filters['sexo'] = $_get['sexo'];
      }
      if(@$_get['edad']!='')
      {
        $filters['edad'] = $_get['edad'];
      }
      if(@$_get['nse']!='')
      {
        $filters['nse'] = $_get['nse'];
      }

      $view_type = 'basic';
      $data_type = 'percent';

      if(@$_get['resultado']!='')
      {
        $resultado = explode('-',$_get['resultado']);
        $view_type = $resultado[0];
        $data_type = $resultado[1];
      }

      $data = null;

      if($_get['medio']!='')
      {

        $builder = new ReportBuilder();

       	$reportBuild = $builder->build('reporting_'.$report->id);

       	$numberOfRespondents = $reportBuild->count();

  	   	$document = $reportBuild->getTable(
              [
                  'filters' => $filters,
                  'mediaType' => 'group_'.$_get['medio'],
                  'view' => [
                      'graphic' => 'table',
                      'type' => $view_type, // lo valores que puede tomar son 'extend' y 'basic'
                      'data' => ['type' => $data_type /*, 'extendValue' => 567*/], // los valores de type que puede tomar son 'numeric' y 'percent', es opcional extendValue
                      'columns' => config('reports.view.columns.'.$_get['medio']), // es opcional, sino se usa devuelve todos.
                      'numberOfRespondents' => $numberOfRespondents, // cantidad de elementos totales
                      //'columns' => ['A veces' => 'A veces', 'Casi siempre' => 'Casi Siempre','Siempre' => 'Siempre'], // es opcional, sino se usa devuelve todos.
                  ]
              ]
          );

        //$document = $this->cleanColumnWithEmptyValues($document);
  	   	$data = [
  	   		'columns' => $this->getColumns($document),
  	   		'values' => $this->getValues(
  	   		    $document,
                ($data_type == 'percent') ? '%s%%' :
                    ( $data_type == 'average' ? '%s%%' : false )
            ),
  	   	];
      }

      $options = $this->getOptionsFilter($client,$report->permissions);

      return view('report',compact('report','data','_get','options','client'));
   }

   public function cleanColumnWithEmptyValues($document)
   {
      $countItems = [];

      foreach ($document as $key => $item) {
        foreach ($item as $k => $val) {
          if(!isset($countItems[$k])) $countItems[$k] = 0;
          $countItems[$k] = $countItems[$k] + ( $val > 0 ? 1 : 0);
        }
      }

      $keysRemove = [];

      foreach ($countItems as $key => $value) {
        if($value == 0) $keysRemove[] = $key;
      }

      if(count($keysRemove) > 0){
        foreach ($document as $key => $item) {
          foreach ($keysRemove as $val) {
            unset($document[$key][$val]);
          }
        }            
      }
  
      return $document;
   }

   public function getOptionsFilter($client, $permissions)
   {
      /*$data = [
        'medio' => [
            'radio', 'television', 'diario', 'internet', 'revista', 'redessociales',
        ],
        'localidad' => [
            'Comodoro Rivadavia','Trelew','Puerto Madryn','Esquel'
        ],
        'sexo' => ['Masculino','Femenino'],
        'edad' => [
          'menos de 17 años','18 a 24 años','25 a 34 años','35 a 44 años','45 a 54 años','más de 54 años'
        ],
        'nse' => ['BC1C2','C3','D1D2E']
      ];

      if($this->report->id == 9 || $this->report->id == 10)
      {
          $data['localidad'] = ['NEUQUEN'];
      }*/


      $builder = new ReportBuilder();

      $reportBuild = $builder->build('reporting_'.$this->report->id);

      $fields_file = $reportBuild->getExistsValues(['localidad','sexo','edad','nse']);

      if(isset($this->report->config['version']) && $this->report->config['version'] == 2){
          $data = array_merge( [ 'medio' => config('reports.medio_2')] , $fields_file );
      }else{
          $data = array_merge( [ 'medio' => config('reports.medio')] , $fields_file );
      }

      $permission = @$permissions[$client];

      $options = [];

      foreach ($data as $key => $items)
      {
          foreach ($items as $k => $value) 
          {
              $keyMedio = (!is_numeric($k) ? $k : $value);
              $options[$key][$k]['name'] = $value;
              $options[$key][$k]['key'] = $keyMedio;

              $options[$key][$k]['status'] = 'enabled';

              if(isset($permission[$key]) && !in_array($keyMedio, $permission[$key]))
              {
                  $options[$key][$k]['status'] = 'disabled';
              }

          }  
      }

      return $options;

   }

   public function getColumns($list)
   {
   		$result = [];

   		$result[] = 'Medio';
   		foreach ($list as $key => $value) {

        if(is_array($value))
        {
          foreach ($value as $k => $val) {
            $result[] = $k;
          }
        }else{
          $result[] = 'Total';
        }

   			break;
   		}

   		return $result;
   }

   public function getValues($list,$format=false)
   {
   		$result = [];
   		$i=0;
   		foreach ($list as $key => $value) {

   			$result[$i][] = (string)$key;

            if(is_array($value))
            {
                    foreach ($value as $k => $val) {
                        $result[$i][] = !$format ? $this->withFactor($val): (object) ['v' => $val, 'f' => sprintf($format,$val)] ;
                    }

            }else
            {
              $result[$i][] = !$format ? $value: (object) ['v' => $value, 'f' => sprintf($format,$val)];
            }
   			$i++;
   		}

   		return $result;
   }

   public function withFactor($val)
   {
      
      $factor = $this->report->config['factor'];

      if(isset($factor[Input::get('medio')][Input::get('localidad')]))
          $val = $val * $factor[Input::get('medio')][Input::get('localidad')];

      return $val;
   }

}