<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\ReportRepository;
use App\Models\Report;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller {

   public function __construct( ReportRepository $reportRepository )
   {
        $this->reportRepository = $reportRepository;
   }


	public function index( Request $request)
    {
    	 return view('welcome');
    }


    public function listReport()
    {
    	$data = $this->reportRepository->search([]);

    	$results = [];

    	foreach ($data->results as $key => $value)
    	{
    		unset($value->config);
    		$results[] = $value;
    	}

    	return response()->json($results);
    }

    public function report( Report $report )
    {
    	unset($report->config);
        $report->token = $report->token;
    	return response()->json($report);
    }
}