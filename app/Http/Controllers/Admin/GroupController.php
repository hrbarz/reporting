<?php namespace App\Http\Controllers\Admin;

use App\Repositories\GroupRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\GroupRequest;
use App\Models\Group;
use Request;

class GroupController extends Controller {

   public $groupRepository;

   public function __construct( GroupRepository $groupRepository )
   {
        $this->groupRepository = $groupRepository;
   }

   public function index( )
   {

      $request = Request::all();

      $search = $this->groupRepository->search( $request, 20 );

      return view('admin.group.index', compact('search'));
   }

   public function create( Group $group )
   {
      return view('admin.group.form',compact('group'));
   }

   public function store( GroupRequest $request )
   {
      $group = $this->groupRepository->create( $request->all() );

      return redirect()->route('admin.group.index');
   }

   public function edit( Group $group )
   {
      return view('admin.group.form', compact('group'));
   }

   public function update( Group $group, GroupRequest $request )
   {
      $this->groupRepository->update( $request->all(), $group->id );
      return redirect()->route('admin.group.index');
   }

   public function delete( Group $group )
   {
      $this->groupRepository->delete( $group->id );
      return redirect()->route('admin.group.index');
   }

}

?>
