<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Request;

class UserController extends Controller {

   public $userRepository;

   public function __construct( UserRepository $userRepository )
   {
        $this->userRepository = $userRepository;
   }

   public function index( $type = false )
   {
       $request = Request::all();
       $request['type'] = $type;

       $search = $this->userRepository->search( $request, 20 );

       // filters
       $status = selectBox(['','active','inactive'] );
       $sorts = selectBox(['','created_at:asc','created_at:desc','updated_at:asc','updated_at:desc']);

       return view('admin.users.index', compact('search', 'status', 'sorts'));
   }

   public function create( User $user )
   {
       return view('admin.users.form', compact('user'));
   }

   public function store( UserRequest $request )
   {
       $this->userRepository->create( $request->all() );
       //return view('admin.users.form', compact('user'));
       return redirect()->route('admin.users');
   }

   public function edit( User $user )
   {
       return view('admin.users.form', compact('user'));
   }

   public function update(  User $user )
   {
      $this->userRepository->update( Request::all() , $user->id );
      return redirect()->route('admin.user.info',$user->id);
   }

   public function view( User $user )
   {
       return view('admin.users.view', compact('user'));
   }






 }

?>
