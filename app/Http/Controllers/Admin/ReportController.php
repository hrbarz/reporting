<?php namespace App\Http\Controllers\Admin;

use App\Repositories\ReportRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReportRequest;
use App\Models\Report;
use App\Models\Perfilcliente;
use Component\Report\ReportBuilder;
use Illuminate\Http\Request;


class ReportController extends Controller {

   public $reportRepository;

   public function __construct( ReportRepository $reportRepository )
   {
        $this->reportRepository = $reportRepository;
   }

   public function index( Request $request )
   {
      $request = $request->all();

      $status = selectBox(['','active','inactive'] );
      $sorts = selectBox(['','created_at:asc','created_at:desc','updated_at:asc','updated_at:desc']);

      $search = $this->reportRepository->search( $request, 20 );

      return view('admin.reports.index', compact('search','status', 'sorts'));
   }

   public function create( Report $report )
   {
      return view('admin.reports.form',compact('report'));
   }

   public function store( ReportRequest $request )
   {
      $report = $this->reportRepository->create( $request->all() );

      return redirect()->route('admin.report.edit',$report->id);
   }

   public function edit( Report $report )
   {

      $type_colums = config('reports.columns');

      $fields = array();

      $factor = @$report->config['factor'];
      
      if(@$report->config['define_columns'] == 1
       // && class_exists('MongoDB') //parche no mongo driver
        ) {

        $builder = new ReportBuilder();

        $reportBuild = $builder->build('reporting_'.$report->id);

        $fields_ = $reportBuild->getExistsValues(['localidad','sexo','edad','nse']);

        $fields = array_merge( [ 'medio' => config('reports.medio')] , $fields_ );

        if(@$report->config['version'] == ReportBuilder::VERSION_MULTI_GROUP){
          $type_colums = config('reports.columns_v2');
        }
      } 

      return view('admin.reports.form', compact('report','type_colums','fields','factor'));
   }

   public function config( Report $report )
   {
      $factor = null;
      $fields_file = null;
      
      if(@$report->config['file'] != '')
      {
        
        $fields_file = @$report->config['columns'];
        $factor = @$report->config['factor'];
      }

      $collection = (new \MongoDB\Client('mongodb://'.config('database.connections.mongodb.host').':'.config('database.connections.mongodb.port')))->{config('database.connections.mongodb.database')}->reporting;

      $type_colums = config('reports.columns');

      return view('admin.reports.form-config', compact('report','fields_file','type_colums'));
   }

   public function configV2( Report $report )
   {
      $factor = null;
      $fields_file = null;
      
      if(@$report->config['file'] != '')
      {
        
        $fields_file = @$report->config['columns'];
        $factor = @$report->config['factor'];
      }

      $collection = (new \MongoDB\Client('mongodb://'.config('database.connections.mongodb.host').':'.config('database.connections.mongodb.port')))->{config('database.connections.mongodb.database')}->reporting;

      $type_colums = config('reports.columns_v2');

      return view('admin.reports.form-config-v2', compact('report','fields_file','type_colums'));
   }

   public function configUpdateV2( Report $report, Request $request )
   {
      $data = $request->all();
      $data['version'] = ReportBuilder::VERSION_MULTI_GROUP;

      $model = $this->reportRepository->update( $data, $report->id );

      if(isset($data['process']) || @$data['check_no_group'] == '1')
      {
         $builder = new ReportBuilder();

         $build = $builder->build(
             'reporting_'.$report->id,
             storage_path('app') . '/' . $model->config['file'],
             $data['version']
         );

         $build->saveReport($model->config['groups']);         
      }
      return redirect()->route('admin.report.configv2',$report->id);
   }


   public function configUpdate( Report $report, Request $request )
   {
      $model = $this->reportRepository->update( $request->all(), $report->id );

      $data = $request->all();

      if(isset($data['process']) || @$data['check_no_group'] == '1')
      {

         $builder = new ReportBuilder();

         $build = $builder->build(
             'reporting_'.$report->id,
             storage_path('app') . '/' . $model->config['file']
         );

         $build->saveReport($model->config['groups']);         
         //print_r($formatted); die;
      }
      return redirect()->route('admin.report.config',$report->id);
   }


   public function permissions(Report $report)
   {
      $perfiles = Perfilcliente::where('Permisos','like','%investigacion%')->get();
      $list = array();
      foreach ($perfiles as $key => $perfil) {
            $codeuser = str_replace('-','_',str_slug($perfil->Nombre));

            $list[$key]['id'] = $perfil->IDPerfilcliente;
            $list[$key]['name'] = $perfil->Nombre;
            $list[$key]['code'] = $codeuser;

      }

      $profiles = json_encode($list);


      $fields = 'null';

      if($report->type == 'medios' /*&& class_exists('MongoDB') */ )
      {
        $builder = new ReportBuilder();

        $reportBuild = $builder->build('reporting_'.$report->id);

        $fields_file = $reportBuild->getExistsValues(['localidad','sexo','edad','nse']);

        $configReportMedio = 'reports.medio';

        if(@$report->config['version'] == ReportBuilder::VERSION_MULTI_GROUP){
            $configReportMedio = 'reports.medio_2';
        }

        $fields = json_encode(array_merge( [ 'medio' => config($configReportMedio)] , $fields_file ));
      }
      /*dd($list);

      $fields = json_encode([
        'medio' => ['radio', 'television', 'diario', 'internet', 'revista', 'redessociales'],
        'localidad' => ['Comodoro Rivadavia','Trelew','Puerto Madryn','Esquel'],
        'sexo' => ['Masculino','Femenino'],
        'edad' => ['menos de 17 años','18 a 24 años','25 a 34 años','35 a 44 años','45 a 54 años','más de 54 años'],
        'nse' => ['BC1C2','C3','D1D2E']
      ]);*/

      $permissions = json_encode( $report->permissions );

      return view('admin.reports.form-permissions',compact('report','profiles','fields','permissions'));

   }

   public function permissionsUpdate( Report $report, Request $request )
   {
      $data = $request->all();

      $data['permissions'] = !isset($data['permissions']) ? [] : $data['permissions'];

      $model = $this->reportRepository->update( $data , $report->id );
      return redirect()->route('admin.report.permissions',$report->id);
   }

   /*public function permissions( Report $report )
   {
       $file = file_get_contents('http://3comunicaciones.com.ar/api-v1/?action=profiles_investigaciones_list');

       $list_profiles = [
          [
            'name' => '3 Comunicaciones',
            'code' => '3_comunicaciones',
          ],
          [
            'name' => 'TreeByte',
            'code' => 'treebyte',
          ],
       ];
       if($file!='' && $data = json_decode($file,true))
       {
          $list_profiles = $data;

       }



      $fields = [
        'medio' => [
            'radio', 'television', 'diario', 'internet', 'revista', 'redessociales',
        ],
        'localidad' => [
            'Comodoro Rivadavia','Trelew','Puerto Madryn','Esquel'
        ],
        'sexo' => ['Masculino','Femenino'],
        'edad' => [
          'menos de 17 años','18 a 24 años','25 a 34 años','35 a 44 años','45 a 54 años','más de 54 años'
        ],
        'nse' => ['BC1C2','C3','D1D2E']
      ];

      return view('admin.reports.form-permissions',compact('report','list_profiles','fields'));
   }*/



   public function update( Report $report, ReportRequest $request )
   {
      $model = $this->reportRepository->update( $request->all(), $report->id );

      return redirect()->route('admin.report.edit',[$report->id]);
   }

   public function delete( Report $report )
   {
      $this->reportRepository->delete( $report->id );
      return redirect()->route('admin.reports');
   }

    public function testReports()
    {
        $filePath = storage_path('app') . '/' . 'BBDD_FINAL_TREEBYTE1.xlsx';

        $builder = new ReportBuilder();
        $report = $builder->build('reporting_9', $filePath);

        $document = $report->getColumns();

        //echo $this->groupsGenerate($document);

        $report->saveReport($this->getFormatConfigBDFinal());


        //$document = $report->getExistsValues(['sexo','edad']);

        /*$document = $report->getTable(
            [
                'filters' => ['sexo' => 'Masculino'],
                'mediaType' => 'group_revista',
                'tableType' => [
                    'graphic' => 'table',
                    //'type' => 'basic',
                    'type' => 'extend',
                    'data' => 'numeric'
                    //'data' => 'percent'
                ]
            ]
        );*/

        print_r('hola');

    }

    protected function groupsGenerate($columns)
    {
        $plantilla = "['name' => 'radio', 'type' => 'MULTICHECK', 'from' => ##from##, 'to' => ##to##],\n";
        $inGroup = false;
        $output = '';

        foreach ($columns as $key => $column) {

            $nextColumn = false;

            if (isset($columns[$key+1])) {
                $nextColumn = explode('-', $columns[$key+1]);
                $nextColumn = $nextColumn[0];
            }

            if (!$inGroup && strpos($column, $nextColumn) !== false) {
                $from = $key;
                $inGroup = true;

                continue;
            }

            if ($inGroup &&  strpos($column, $nextColumn) === false) {
                $outputPlantilla = $plantilla;
                $outputPlantilla = str_replace('##from##', $from, $outputPlantilla);
                $outputPlantilla = str_replace('##to##', $key, $outputPlantilla);


                $output .= $outputPlantilla;

                $inGroup = false;

                continue;
            }
        }

        return $output;
    }



   protected function getFormatterConfig()
   {
      return [
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 14, 'to' => 29],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 30, 'to' => 45],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 46, 'to' => 61],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 62, 'to' => 77],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 78, 'to' => 93],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 94, 'to' => 109],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 110, 'to' => 125],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 126, 'to' => 141],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 142, 'to' => 157],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 158, 'to' => 173],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 174, 'to' => 189],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 190, 'to' => 205],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 206, 'to' => 221],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 222, 'to' => 237],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 238, 'to' => 253],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 254, 'to' => 269],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 270, 'to' => 285],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 286, 'to' => 301],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 302, 'to' => 317],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 318, 'to' => 333],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 334, 'to' => 349],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 350, 'to' => 365],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 366, 'to' => 381],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 382, 'to' => 397],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 398, 'to' => 413],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 414, 'to' => 429],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 430, 'to' => 461],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 462, 'to' => 477],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 478, 'to' => 493],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 494, 'to' => 509],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 510, 'to' => 525],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 526, 'to' => 541],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 542, 'to' => 557],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 558, 'to' => 573],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 574, 'to' => 589],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 590, 'to' => 605],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 606, 'to' => 621],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 622, 'to' => 637],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 638, 'to' => 653],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 654, 'to' => 669],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 670, 'to' => 685],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 686, 'to' => 701],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 702, 'to' => 717],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 718, 'to' => 733],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 734, 'to' => 749],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 750, 'to' => 765],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 766, 'to' => 781],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 782, 'to' => 797],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 798, 'to' => 813],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 814, 'to' => 829],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 830, 'to' => 845],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 846, 'to' => 861],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 862, 'to' => 877],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 878, 'to' => 893],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 894, 'to' => 909],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 910, 'to' => 925],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 926, 'to' => 941],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 942, 'to' => 957],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 958, 'to' => 973],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 974, 'to' => 989],
          ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 990, 'to' => 1005],

          ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1006, 'to' => 1012],
          ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1013, 'to' => 1019],
          ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1020, 'to' => 1026],
          ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1027, 'to' => 1033],
          ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1034, 'to' => 1040],
          ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1041, 'to' => 1047],
          ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1048, 'to' => 1054],

          ['name' => 'revista', 'type' => 'VALUE', 'from' => 1055, 'to' => 1063],

          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1064, 'to' => 1079],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1080, 'to' => 1095],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1096, 'to' => 1111],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1112, 'to' => 1127],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1128, 'to' => 1143],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1144, 'to' => 1159],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1160, 'to' => 1175],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1176, 'to' => 1191],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1192, 'to' => 1207],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1208, 'to' => 1223],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1224, 'to' => 1239],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1240, 'to' => 1255],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1256, 'to' => 1287],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1288, 'to' => 1303],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1304, 'to' => 1319],
          ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1320, 'to' => 1335],

          ['name' => 'internet', 'type' => 'VALUE', 'from' => 1336, 'to' => 1358],

          ['name' => 'redessociales', 'type' => 'CHECK', 'from' => 1359, 'to' => 1376],

      ];
   }

    protected function getFormatConfigBDFinal()
    {
        return [

            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 14, 'to' => 29],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 30, 'to' => 45],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 46, 'to' => 61],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 62, 'to' => 77],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 78, 'to' => 93],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 94, 'to' => 109],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 110, 'to' => 125],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 126, 'to' => 141],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 142, 'to' => 157],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 158, 'to' => 173],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 174, 'to' => 189],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 190, 'to' => 205],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 206, 'to' => 221],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 222, 'to' => 237],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 238, 'to' => 253],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 254, 'to' => 269],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 270, 'to' => 285],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 286, 'to' => 301],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 302, 'to' => 317],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 318, 'to' => 333],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 334, 'to' => 349],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 350, 'to' => 365],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 366, 'to' => 381],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 382, 'to' => 397],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 398, 'to' => 413],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 414, 'to' => 429],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 430, 'to' => 445],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 446, 'to' => 461],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 462, 'to' => 477],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 478, 'to' => 493],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 494, 'to' => 509],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 510, 'to' => 525],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 526, 'to' => 541],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 542, 'to' => 557],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 558, 'to' => 573],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 574, 'to' => 589],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 590, 'to' => 605],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 606, 'to' => 621],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 622, 'to' => 637],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 638, 'to' => 653],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 654, 'to' => 669],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 670, 'to' => 685],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 686, 'to' => 701],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 702, 'to' => 717],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 718, 'to' => 733],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 734, 'to' => 749],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 750, 'to' => 765],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 766, 'to' => 781],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 782, 'to' => 797],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 798, 'to' => 813],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 814, 'to' => 829],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 830, 'to' => 845],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 846, 'to' => 861],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 862, 'to' => 877],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 878, 'to' => 893],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 894, 'to' => 909],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 910, 'to' => 925],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 926, 'to' => 941],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 942, 'to' => 957],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 958, 'to' => 973],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 974, 'to' => 989],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 990, 'to' => 1005],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 1006, 'to' => 1021],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 1022, 'to' => 1037],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 1038, 'to' => 1053],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 1054, 'to' => 1069],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 1070, 'to' => 1085],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 1086, 'to' => 1101],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 1102, 'to' => 1117],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 1118, 'to' => 1133],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 1134, 'to' => 1149],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 1150, 'to' => 1165],

            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1166, 'to' => 1172],
            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1173, 'to' => 1179],
            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1180, 'to' => 1186],
            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1187, 'to' => 1193],
            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1194, 'to' => 1200],
            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1201, 'to' => 1207],
            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1208, 'to' => 1214],

            ['name' => 'revista', 'type' => 'VALUE', 'from' => 1215, 'to' => 1223],

            ['name' => 'mediosonline', 'type' => 'VALUE', 'from' => 1504, 'to' => 1529],

            ['name' => 'redessociales', 'type' => 'CHECK', 'from' => 1530, 'to' => 1547],

        ];
    }
}

?>
