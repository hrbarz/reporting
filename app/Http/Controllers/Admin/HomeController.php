<?php namespace App\Http\Controllers\Admin;

use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;

class HomeController extends Controller {

    public function index()
    {
       return view('admin.home');
    }

    public function settings()
    {
       return view('admin.settings.index');
    }

    public function market()
    {
       return view('admin.market.index');
    }

    public function community()
    {
       return view('admin.community.index');
    }

    public function analytics()
    {
       return view('admin.analytics.index');
    }

}

?>
