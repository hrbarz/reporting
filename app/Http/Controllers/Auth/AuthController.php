<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Session;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Repositories\UserRepository;
use App\Http\Requests\UserRequest;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    public $userRepository;


    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository )
    {
        $this->userRepository = $userRepository;

        $this->middleware($this->guestMiddleware(), ['except' => ['logout', 'token']] );
       // $this->middleware('guest', ['except' => ['logout' , 'token', 'facebook'] ] );
    }

    public function login()
    {
          return view('welcome');
    }

    public function signup()
    {
          return view('user.signup');
    }

    public function logout()
    {
          Auth::logout();
          return redirect()->back();
    }


    public function authenticate( Request $request )
    {

          if (Auth::guard('web')->attempt(['email' => $request->get('email'), 
                             'password' => $request->get('password'),
                             'status'=>'active'], 
                             $request->get('remember')))
          {

              return redirect()->back();

          }

          return redirect()->back()->withErrors( ['login' => trans('app.login-error')] );
    }

    public function store( UserRequest $request )
    {
        $data = [ 'first_name' => $request->input('first_name'),
                  'last_name' => $request->input('last_name'),
                  'email' => $request->input('email'),
                  'status' => 'inactive',
                  'password' => $request->input('password') ];

        if($user = $this->userRepository->create($data))
        {
            Auth::login($user, true);
        }
        
          return redirect()->back();

    }

     public function token()
    {
        
        if( !isset($_GET['token']) )
        {
           Alert::danger( trans('app.token-invalid') );
           return redirect()->route('home');
        }

        $user = User::where('token','=', $_GET['token'] )->first();
        
        if( !$user )
        {
           Alert::danger('Token Invalid');
           return redirect()->route('home');
        }
        
        if( $user->status == 'inactive' )
        {
           Alert::success( trans('app.login-activated') );
           $user->status = 'active';
           $user->save();
        }
        
        Auth::login($user, true);
        
        if( !isset( $_GET['redirect'] ) )
        {
           $_GET['redirect'] = route('home');
        }

        return redirect( urldecode( $_GET['redirect'] ) );
      
    }

}