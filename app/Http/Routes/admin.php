<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/test',function(){

	dd(index_case('Identificación del encuestado')); die;

	$collection = (new MongoDB\Client('mongodb://'.config('database.connections.mongodb.host').':'.config('database.connections.mongodb.port')))->{config('database.connections.mongodb.database')}->reporting;

});

Route::get('/dashboard'			, ['as' => 'dashboard'	, 'uses' => 'Admin\HomeController@index']);

/* users */
Route::get('/user/create'		, ['as' => 'user.create', 'uses' => 'Admin\UserController@create']);
Route::post('/user/create'		, [						  'uses' => 'Admin\UserController@store']);
Route::any('/users/{type?}'		, ['as' => 'users'		, 'uses' => 'Admin\UserController@index']);
Route::get('/user/{user}'		, ['as' => 'user.info'	, 'uses' => 'Admin\UserController@view']);
Route::get('/user/{user}/edit'	, ['as' => 'user.edit'	, 'uses' => 'Admin\UserController@edit']);
Route::post('/user/{user}/edit' , [						  'uses' => 'Admin\UserController@update']);


/*reports*/
Route::any('/reports'				, ['as' => 'reports'		, 'uses' => 'Admin\ReportController@index']);
Route::get('/report/create'			, ['as' => 'report.create'	, 'uses' => 'Admin\ReportController@create']);
Route::post('/report/create'		, [						 	  'uses' => 'Admin\ReportController@store']);
Route::get('/report/{report}/delete',['as' => 'report.delete','uses' => 'Admin\ReportController@delete']);
Route::get('/report/{report}/edit'	, ['as' => 'report.edit', 'uses' => 'Admin\ReportController@edit']);
Route::post('/report/{report}/edit'	, [						  'uses' => 'Admin\ReportController@update']);
Route::get('/report/{report}/config', ['as' => 'report.config', 'uses' => 'Admin\ReportController@config']);
Route::get('/report/{report}/config-v2', ['as' => 'report.configv2', 'uses' => 'Admin\ReportController@configV2']);
Route::post('/report/{report}/config' , [             'uses' => 'Admin\ReportController@configUpdate']);
Route::post('/report/{report}/config-v2'	, [						  'uses' => 'Admin\ReportController@configUpdateV2']);
Route::get('/report/{report}/permissions', ['as' => 'report.permissions', 'uses' => 'Admin\ReportController@permissions']);
Route::post('/report/{report}/permissions'	, [						  'uses' => 'Admin\ReportController@permissionsUpdate']);



Route::get('/testreports'			, ['as' => 'testreports'	, 'uses' => 'Admin\ReportController@testReports']);
Route::get('/reports/get'			, ['as' => 'report.get'	, 'uses' => 'Admin\ReportController@get']);
