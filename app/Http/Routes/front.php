<?php


Route::get('/admin/login', ['as' => 'home', 'uses' => 'Front\HomeController@index']);

Route::get('front/report/{report}',['as' => 'report', 'uses' => 'Front\ReportController@view'] );

// AUTH
Route::get('auth/signup',     ['as' => 'auth.signup', 'uses' => 'Auth\AuthController@signup']);
Route::post('auth/signup',    ['uses' => 'Auth\AuthController@store']);
Route::get('auth/login',      ['as' => 'auth.login', 'uses' => 'Auth\AuthController@login']);
Route::post('auth/login',     ['uses' => 'Auth\AuthController@authenticate']);
Route::get('auth/logout',     ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);
Route::get('auth/token',      ['as' => 'token', 'uses' => 'Auth\AuthController@token']);

Route::get('/api/list-report', ['as' => 'list-report', 'uses' => 'Front\HomeController@listReport']);
Route::get('/api/report/{report}', ['as' => 'report-api', 'uses' => 'Front\HomeController@report']);
