<?php 

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\User;
use Validator;
use Input;
use Sendy;

class UserRepository extends BaseRepository {

  public $model   = 'App\Models\User';
  public $filters_hidden  = ['type'];
  public $export  = ['first_name','last_name','email','type','status','created_at'];


  /* Search Filters */
  public function typeFilter($query, $value)
  {
     return $query->where('type','=', $value);
  }

  public function searchFilter( $query, $value )
  {
      $query->where(function( $query ) use ($value) {

        $query->where('first_name','LIKE', '%'. $value .'%')
              ->orWhere('last_name','LIKE', '%'. $value .'%')
              ->orWhere('email','LIKE', '%'. $value .'%');
      });

      return $query;
  }

  public function genderFilter($query, $value)
  {
     return $query->where('gender','=', $value);
  }  

  public function geoFilter($query, $value)
  {
     return $query->where('geo_id','=', $value);
  }  

  public function exists( $email )
	{
  		$validator = Validator::make(
  		    array('email' => $email),
  		    array('email' => 'unique:users,email')
  		);

  		if ($validator->fails())
  		{
  			   return User::where('email','=', $email)->first();
  		}

  		return false;
	}

  public function create( array $data )
  {

     $data['token'] = token();

     if( isset($data['password']) && $data['password'] != '' )
     {
        $data['password'] = bcrypt($data['password']); 
     }

     $model = parent::create( $data );

     // register in sendy
     $data['name'] = $model->full_name;
     $data['created_at'] = $model->created_at;

     return $model;
  }

  public function update( array $data, $id )
  {

     if(@$data['photo_url'])
     {
        $data['photo_id'] = $this->attach($data['photo_url']);
     }



     if( isset($data['password']) && $data['password'] != '' )
     {
        $data['password'] = bcrypt($data['password']); 
     }

     
     if(!is_null( Input::file('photo_id')))
     {
         $data['photo_id'] = $this->attach( @Input::file('photo_id') );
     }

     if( isset( $data['birthday_day'] ) )
     {
        $data['birthdate'] = $data['birthday_year'] . '-' . $data['birthday_month'] .'-'. $data['birthday_day'] ;
     }

     $model = parent::update( $data , $id );
     $this->users_languages( @$data['languages'], $id );
     $this->calendar_day_of_week( @$data['calendar_day_of_week'], $id );
     $this->calendar_update( @$data['calendar'], $id );

     return $model;
  }

  public function users_languages($languages, $user_id)
  {

      if(!$languages){ return false; }

      UserLanguage::where('user_id', $user_id)->delete();

      if(is_array($languages) )
      {
          foreach($languages as $language_id)
          {
              UserLanguage::create(['user_id' => $user_id, 'language_id' => $language_id]);
          }
      }

      return true;

  }

  public function calendar_day_of_week( $days, $user_id )
  {
      if(!$days){ return false; }

      Calendar::where('user_id', $user_id)->where('event','day')->delete();

      foreach($days as $day)
      {
         Calendar::create(['user_id' => $user_id, 'event' => 'day', 'content' => $day]);
      }

      return true;
  }

  public function calendar_update( $calendar, $user_id )
  {
      if(!$calendar){ return false; }

      Calendar::where('user_id', $user_id)->where('event','date')->delete();
      $calendar = explode(',', $calendar);
      foreach($calendar as $date)
      {
         Calendar::create(['user_id' => $user_id, 'event' => 'date', 'content' => $date]);
      }

      return true;
  }



}

?>
