<?php 

namespace App\Repositories;

use App\Repositories\BaseRepository;
use Request;
use Auth;

class MediaRepository extends BaseRepository {

    public $model   = 'App\Models\Media';
    public $export  = ['id','format','path','caption','created_at'];


	public function userFilter($query,$value)
	{

	  $query->where(function( $query ) use ($value) {

	      $query->where('user_id','=', $value ); 

	  });

	  return $query;
	}

    public function create( array $data )
    {
       
       $data['user_id'] = Auth::id();

       return parent::create( $data );

    }    


}

?>
