<?php 

namespace App\Repositories;

use App\Repositories\BaseRepository;
use Str;
use Input;
use App\Helpers\Search\GroupView;

class GroupRepository extends BaseRepository {

  public $model   = 'App\Models\Group';
  public $export  = ['id','name'];


  public function searchFilter( $query, $value )
  {
      $query->where(function( $query ) use ($value) {

        $query->where('name','LIKE', '%'. $value .'%');
      });

      return $query;
  }

  public function groupView( $group, $data )
  {
      $groupView = new GroupView( $this, $group );

      $groupView->run();
      
      if( isset($data['export']) )
      {
          return $groupView->export();
      }

     // dd($groupView);
      return $groupView;
  }

  public function create( array $data )
  {

     $model = parent::create( $data );

     return $model;
  }

  public function update( array $data, $id )
  {

    $model = parent::update( $data, $id );

    return $model;
  }

}

?>
