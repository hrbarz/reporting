<?php 

namespace App\Repositories;

use App\Repositories\BaseRepository;
use Str;
use Input;
use App\Helpers\Search\ReportView;

use Component\Report\ReportBuilder;
use Component\Report\ReportFormatterBuilder;
use Illuminate\Contracts\Filesystem\Filesystem;

class ReportRepository extends BaseRepository {

  public $model   = 'App\Models\Report';
  public $export  = ['id','title'];


  public function searchFilter( $query, $value )
  {
      $query->where(function( $query ) use ($value) {

        $query->where('title','LIKE', '%'. $value .'%');
      });

      return $query;
  }

  public function reportView( $report, $data )
  {
      $reportView = new ReportView( $this, $report );

      $reportView->run();
      
      if( isset($data['export']) )
      {
          return $reportView->export();
      }

     // dd($reportView);
      return $reportView;
  }

  public function create( array $data )
  {

     $model = parent::create( $data );

     return $model;
  }

  public function update( array $data, $id )
  {
    $data['version'] = isset($data['version']) ? $data['version'] : ReportBuilder::VERSION_STANDARD;

    if(@$data['filereport'])
    {
      $file = $data['filereport'];
      $s3 = \Storage::disk('s3');
      $filePath = '/reports/' .  str_slug($data['title']). '.' . $file->getClientOriginalExtension();
      $s3->put($filePath, file_get_contents($file), 'public');

      $data['filereport'] = $filePath;
    }

    if(@$data['filereportremove'] == '1') $data['filereport'] = '';


    $model = parent::update( $data, $id );

    $config = [];

    if($file = $this->attachFile( @Input::file('file')))
    {
        $config['file'] = $file->path;

        $builder = new ReportBuilder();
        $report = $builder->build(
            'reporting_'.$id,
            storage_path('app') . '/' . $config['file'],
            $data['version']
        );

        $columns = $report->getColumns(true);

        if(is_array($columns) && count($columns) > 0)
        {
           $config['columns'] = $columns;
        }

        $config['define_columns'] = false;
        $config['groups'] = [];
    }

    if(isset($data['process']) || @$data['check_no_group'] == '1')
    {
      
      $config['define_columns'] = true;

      if(isset($data['process']))
      {
         $config['groups'] = data_post_group_report($data['process']);
      }
      
      if(@$data['check_no_group'] == '1')
      {
        $config['groups'] = [];
      }

    }

    if(isset($data['factor']))
    {
      $config['factor'] = $data['factor'];
    }

    if(isset($data['version']))
    {
      $config['version'] = $data['version'];
    }

    if(count($config) > 0)
    {
        $old_config = (@$model->config) ? $model->config : [];

        $model->config = array_merge($old_config,$config);
    debug($model);
        $model->save();
    }

    return $model;
  }

}

?>
