<?php 

namespace App\Repositories;

use App\Helpers\Search\Search;
use App\Helpers\FileManager;
use Auth;
use Str;

abstract class BaseRepository {


    public    $model;
    public    $export = ['id','updated_at','created_at'];
    public    $filters_hidden;
    public    $orderBy = 'id:desc';


    public function __construct()
    {
       $this->model =  app()->make( $this->model );
    }

    public function getById($id)
  	{
  		 return $this->model->findOrFail($id);
  	}

    public function all()
    {
        return $this->model->all();
    }

    public function update( array $data, $id )
    {
      $this->model = $this->model->find($id);
      $this->model->update( $data );
      return $this->model;
    }

    public function create( array $data )
    {
       return $this->model = $this->model->create( $data );
    }

    public function delete( $id )
  	{
  		 return $this->model->find($id)->delete();
  	}

    


    /* Search */
    public function search( $data =false, $limit=false, $cache=false )
    {
        $search = new Search( $this );
        $search->setFilters( $data );

        if( !isset($data['sort']))
        {
            $search->setFilter('sort', $this->orderBy);
        }
        
        $search->run( $limit, $cache );

        if( isset($data['export']) )
        {
            return $search->export();
        }

        return $search;
    }

    

    // Search Default Filters
    public function globalFilter( $query )
    {
        return $query;
    }

    public function statusFilter( $query, $value )
    {
        return $query->where('status','=', $value);
    }

    public function notInFilter( $query, $ids )
    {
        return $query->whereNotIn('id', $ids);
    }

    public function InFilter( $query, $ids )
    {
        return $query->whereIn('id', $ids);
    }

    public function selectFilter( $query, $value )
    {
        return $query->select($value);
    }
    
    public function typeFilter( $query, $value )
    {
        return $query->where('type','=', $value);
    }
    
    public function langFilter( $query, $value )
    {
        return $query->where('language_id','=', $value);
    }

    public function createdAtFilter( $query, $value )
    {
        return $query->where('created_at','>=', $value);
    }

    public function updatedAtFilter( $query, $value )
    {
        return $query->where('updated_at','>=', $value);
    }

    public function sortFilter( $query, $value )
    {
        $value = explode(':', $value);
        $field = $value[0];
        $sort  = ( isset($value[1]) ? $value[1] : 'desc' );

        $table = $this->model->getTable().'.';

        if( Str::contains( $field, '#' ) )
        {
            $table = '';
            $field = str_replace('#','', $field);
        }

        return $query->orderBy( $table . $field, $sort );
    }

    public function syncFilter( $query, $value )
    {
       $query->where('sync_at', '>', $value);
       return $query;
    }


    public function attach( $file, $model=false )
    {

        if( $model )
        {
            return $this->attachToModel( $file, $model );
        }

        return $this->attachToField( $file );
    }

    public function attachToField( $file )
    {

        if( is_null($file) ){ return false; }

        if( $media = $this->attachFile( $file ) )
        {
            return $media->id;
        }
                   
        return false;

    }

    public function attachToModel( $files, $model )
    {
        if( is_null($files) || is_null($files[0]) ){ return false; }
        
        $attach = [];

        foreach( $files as $file )
        {
            if( $media = $this->attachFile( $file, $model->id ) )
            {
                $attach[] = $media;
            }
        }

        return $attach;

    }

    public function attachFile( $file, $relation_id=0 )
    {
        $mediaRepository   =  new MediaRepository;
        $fileManager = new FileManager();

        if(is_object($file))
        {
            $path = $fileManager->upload( $file );
        }
        else
        {
            $path = $fileManager->download( $file );
        }

        if( $path )
        {
            $data = ['path' => $path,
                     'format' => $fileManager->format( $path ),
                     'relation' => $this->model->getTable(),
                     'relation_id' => $relation_id,
                     'position' => $mediaRepository->model->totalGroup($this->model->getTable(), $relation_id) + 1 ];

            return $mediaRepository->create( $data );
        }

        return false;
    }

    public function media_position($positions)
    {
        if (count($positions) > 0 ) 
        {            
            foreach ($positions as $media_id => $value)
            {
                $media = \App\Models\Media::find($media_id);
                $media->position = $value;
                $media->save();
            }
        }
    }

    public function request_clean( $data )
    {
        foreach( $this->model->casts as $key=>$value )
        {
            if( $value == 'array' && isset($data[$key]) )
            {
                $data[$key] = array_clean( $data[$key] );
            }
        }

        return $data;
    }


 
}
?>
