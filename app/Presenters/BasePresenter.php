<?php

namespace App\Presenters;

use App\Helpers\Filters\Filters;
use App\Repositories\ProductRepository;
use App\Models\Guidebook;
use App\Models\Geo;
use App\Models\Photo;
use App\Models\Media;
use App\Models\Wishlist;
use App\Models\Review;
use App\Models\Like;
use LaravelLocalization;
use Carbon\Carbon;
use Auth;
use Str;

trait BasePresenter
{

    public function scopeHasLanguage( $query )
    {
        $query->where( $this->getTable() . '.language_id','=',LaravelLocalization::getCurrentLocale());

        return $query;
    }
    /**
     * Format created_at attribute
     *
     * @param Carbon  $date
     * @return string
     */
    public function getCreatedAtAttribute($date)
    {
      return $this->getDateFormated($date);
    }

    /**
     * Format updated_at attribute
     *
     * @param Carbon  $date
     * @return string
     */
    public function getUpdatedAtAttribute($date)
    {
      return $this->getDateFormated($date);
    }

    /**
     * Format date
     *
     * @param Carbon  $date
     * @return string
     */
    private function getDateFormated($date)
    {
      return Carbon::parse($date)->format(config('app.locale') == 'fr' ? 'd/m/Y' : 'm/d/Y');
    }

    private function getToDateFormat($date)
    {
      return Carbon::parse($date)->format('Y-m-d');
    }

    public function getUpdatedAtForHumansAttribute()
    { 
      if(!is_route_path('admin.')) Carbon::setLocale(LaravelLocalization::getCurrentLocale());
      return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['updated_at'])->diffForHumans();
    }

    public function getCreatedAtForHumansAttribute()
    {
      if(!is_route_path('admin.')) Carbon::setLocale(LaravelLocalization::getCurrentLocale());
      return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->diffForHumans();
    }

    public function getSyncAtForHumansAttribute()
    {
      if( !$this->attributes['sync_at'] ){ $this->attributes['sync_at'] = $this->attributes['created_at']; }
      if(!is_route_path('admin.')) Carbon::setLocale(LaravelLocalization::getCurrentLocale());
      return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['sync_at'])->diffForHumans();
    }

    /**
     * Generate Nick 1 Character in base of attributes : first_name, last_name, title, name
     *
     * @return string
     */
    public function getNickAttribute()
    {

      if( isset( $this->attributes['last_name'] ) )
      {
          $code = $this->last_name;
      }
      else if( isset( $this->attributes['first_name'] ) )
      {
          $code = $this->first_name;
      }
      else if( isset( $this->attributes['name'] ) )
      {
          $code = $this->name;
      }
      else if( isset( $this->attributes['title'] ) )
      {
          $code = $this->title;
      }


      $name = substr($code, 0, 1);
  		$name = strtoupper($name);

  		return $name;
    }



    /**
     * Check in field content if exist data filter for front view
     *
     * @return string
     */
    public function getContentAttribute($content)
    {        
        $content = str_replace('&nbsp;',' ',$content);


        if( is_route_path('admin.') || is_route_path('settings.blog.') ||  $this->table == 'filters')
        {
            return $content; 
        }

         if( is_route_path('view') && !is_route_path('guidebook'))
        {
            $content = filter_security($content);
        }

        $content = datatable( $content );

        
        if( Str::contains($this->getTable(), 'guidebook') || Str::contains($this->getTable(), 'page')  )
        {
            return links($content);
        }

        return $content;
    }

    public function the_content()
    { 
        $content = $this->content;

        $parse_places = parse_places_in_text($content);

        if($parse_places['@'] || $parse_places['#'])
        {
            foreach ($parse_places['@']  as $key => $value)
            {

                if($value['relation'] == 'guidebook')
                {
                    $place = Guidebook::find($value['id']);
                    $url = $place->url();
                    //$title = $place->title;
                }else
                {
                    $place = Geo::find($value['id']);
                    $url = $place->url('things-to-do.sights');
                    //$title = $place->trans('name');
                }
                $content = str_replace( get_tpl_places('@', $value) , get_link_places([ 'url' => $url , 'title' => $value['title'] ]) , $content);
            }
          
            $index = 1;
            foreach ($parse_places['#']  as $key => $value)
            {

                if($value['relation'] == 'guidebook')
                {
                    $item = Guidebook::where('slug',$value['slug'])->first();
                    $url = $item->url();
                    $title = $item->title;
                    $view = view('front.partials.guidebook-hash', compact('item', 'index'));

                }
                else
                {
                    $item = Geo::where('slug',$value['slug'])->first();
                    $url = $item->url('things-to-do.sights');
                    $title = $item->trans('name');
                    $view = view('front.partials.geo-hash', compact('item', 'index'));
                }

                $index++;

                $content = str_replace( get_tpl_places('#', $value) , $view  , $content);
            }

        }

        return $content;
    }
    
    public function places()
    {
       $list_places = parse_places_in_text($this->content);
       $list = [];

       foreach ($list_places['@'] as $key => $value)
       {
            if($value['relation'] == 'guidebook')
            {
                $list[] = Guidebook::find($value['id']);
            }
       }

       foreach ($list_places['#'] as $key => $value)
       {
            if($value['relation'] == 'guidebook')
            {
                $list[] = Guidebook::where('slug',$value['slug'])->first();
            }
       }

       return $list;
    }

    /**
     * Return photos relationship with the actual model
     *
     * @return Photos Collection
     */
    public function photos()
    {
        return \App\Models\Media::where('relation','=',$this->getTable())
                               ->where('format','=','photo')
                               ->where('relation_id','=',$this->id)
                               ->orderBy('position')
                               ->get();
    }

    public function media()
    {
        return \App\Models\Media::where('relation','=',$this->getTable())
                                ->where('relation_id','=',$this->id)
                                ->orderBy('position')
                                ->get();
    }


    public function cover()
    {
        if( $this->photo_id )
        {
            return $this->photo;
        }

        $photos = $this->photos();
        
        if( count($photos) < 1 )
        {
          return false;
        }

        return $photos[0];
    }

    public function mediaExists( $id )
    {

        return \App\Models\Media::exists($id);

    }

    public function belongsMediaTo( $field )
    {

        $relation = $this->belongsTo('App\Models\Media', $field);
        $media = $relation->getResults();

        if( !$media || !$media->exists )
        {
           if( @$media->id )
           {
              $this->$field = '0';
              $this->save();
           }

           return $relation;
        }

        return $relation;
    }

    public function getPhotoDefault()
    {
        return Media::where('path','planetyze/'.$this->table.'.jpg')->first();
    }

    public function getNickColorAttribute()
    {

        $colors = [
                  'q' => '#d71d28', 
                  'w' => '#f08650', 
                  'e' => '#49f51b', 
                  'r' => '#24e430', 
                  't' => '#3ea274', 
                  'y' => '#2c3ce4', 
                  'u' => '#0a4bc2', 
                  'i' => '#1bb522', 
                  'o' => '#c5792b', 
                  'p' => '#2d18b7', 
                  'a' => '#13565e', 
                  's' => '#ad0248', 
                  'd' => '#11d000', 
                  'f' => '#aaefcd', 
                  'g' => '#8894b2', 
                  'h' => '#eaf2fd', 
                  'j' => '#5b6175', 
                  'k' => '#f823bb', 
                  'l' => '#360f1b', 
                  'z' => '#2d1028', 
                  'x' => '#d52e3d', 
                  'c' => '#61e7d8', 
                  'v' => '#b24f12', 
                  'b' => '#cb4122', 
                  'n' => '#e23267', 
                  'm' => '#85df14'];


      return $colors[ strtolower($this->nick) ];

    }

    public function inWishlist()
    { 
        if( !Auth::check() ){ return false; }

        return Wishlist::where('user_id', Auth::id())->where('relation', $this->getTable())->where('relation_id', $this->id)->first();
    }

    public function isLiked()
    { 
        if( !Auth::check() ){ return false; }

        return Like::where('user_id', Auth::id())->where('relation', $this->getTable())->where('relation_id', $this->id)->first();
    } 

    public function likes()
    {
        return Like::where('relation', $this->getTable())->where('relation_id', $this->id)->count();
    }

    public function _reviews()
    {
        return  Review::where('relation','=',$this->table)->where('relation_id','=',$this->id);
    }

    public function hasReviews($user_id = false)
    {
        $query =  $this->_reviews();

        if($user_id) $query->where('user_id','=',$user_id);

        return $query->count();
    }

    public function reviews()
    {
        return $this->_reviews()->orderBy('id','DESC')->get();
    }

    public function reviewsAverage()
    {
        $reviews = $this->reviews();
        $total = 0;

        if(count($reviews) > 0 )
        {
          foreach ($reviews as $key => $value)
          { 
            $total += $value->rate;
          }

          $avg = $total / count($reviews);

          return number_format($avg , 1);
        }

        return 0;
    }

    public function qualification()
    {
        $reviews = $this->reviews();

        $total = $reviews->count();

        $qualification = [1 => 0, 2 => 0, 3 => 0, 4 =>0 , 5 =>0];
        if(count($reviews) > 0 )
        {
          
          foreach ($reviews as $key => $value)
          { 
          
            $qualification[$value->rate]++;
          
          }

          $result = [];
          
          foreach ($qualification as $key => $value)
          {
          
              $result[$key]['avg'] = decimal(( $value / $total ) * 100  , 0);   
              $result[$key]['count'] = $value;   
          
          }

          return $result;
        
        }

        return false;

    }

    public function createComment($request)
    {
        Review::create([
            'relation' => $this->getTable() , 
            'relation_id' => $this->id , 
            'user_id' => Auth::id(),
            'content' => $request['content'] , 
        ]);

    }    

    public function meta( $field )
    {
      if( isset( $this->meta[$field] ) )
      {
        return $this->meta[$field];
      }

      return false;
    }

    public function hasMeta( $field=false )
    {
        if( !$field )
        {
            return config('meta.' . $this->meta_id);
        }

        $hideFields = config('meta.' . $this->meta_id);

        if( !is_array($hideFields) )
        {
            return true;
        }

        if( in_array( $field, $hideFields ) )
        {
            return false;
        }

        return true;

    }

    public function getField( $field )
    {
       if( $this->$field )
       {
          return $this->$field;
       }

       if( $this->parent_id )
       {
          return $this->parent->$field; 
       }

       return false;       
    }

    public function price()
    {
        if( !($this->id) )
        {
            return false;
        }

        $finance = new \App\Helpers\CurrencyConverter();
        $r = $finance->calculator( $this->price, $this->currency_id, session('currency_id') );

        return $r;
    }

    public function total()
    {
        $finance = new \App\Helpers\CurrencyConverter();
        $r = $finance->calculator( $this->total, $this->currency_id, session('currency_id') );

        return $r;
    }

    public function currency()
    {
        return session('currency_id');
    }

    public function track($field,$total = 1,$one_per_session=false)
    {
        $field_session = $this->table.':'.$field.':'.$this->id;

        if($one_per_session && session($field_session)) return false;
        

        $this->$field = $this->$field + $total;
        $this->save();
        
        if($one_per_session) session([$field_session => true]); 

        return true;

    }

    public function facebook_share()
    {
        return 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode( $this->url() );
    }

    public function twitter_share()
    {
        return 'https://twitter.com/home?status=' . urlencode( $this->url() );
    }

    public function pictures()
    {
        $photo = $this->cover();

        if( !$photo ){  return false; }

        return [
                  'original' => $photo->url(),
                  'xs'       => $photo->xs(),
                  'md'       => $photo->md(),
                  'lg'       => $photo->lg(),
               ];
    }


}
