<?php namespace App\Presenters;

use Carbon\Carbon;
use App\Presenters\BasePresenter;
use Str;

trait UserPresenter {

   use BasePresenter;

   public function isAdmin()
    {

        switch( $this->type )
        {
            case 'support':
            case 'admin':
            case 'super-admin';
                return true;                
        }

        return false;

    }


    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->first_name;
    }

    public function getNameAttribute()
    {
        return $this->getFullNameAttribute();
    }
        
    public function autoLogin()
    {
        return $this->tokenUrl(NULL);
    }
}
