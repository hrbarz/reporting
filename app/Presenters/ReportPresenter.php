<?php namespace App\Presenters;

use Carbon\Carbon;
use App\Presenters\BasePresenter;
use Hash;

trait ReportPresenter {

  use BasePresenter;

  public function getTokenAttribute()
  {
  	 return Hash::make('report_3ce_'.$this->id);
  }
}
