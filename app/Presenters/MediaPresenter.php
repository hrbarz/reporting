<?php namespace App\Presenters;

use Carbon\Carbon;
use App\Presenters\BasePresenter;
use Storage;
use Image;

trait MediaPresenter {

   use BasePresenter;


   public function totalGroup($relation=null, $relation_id=null)
   {
   		return self::where('relation',$relation?:$this->relation)->where('relation_id',$relation_id?:$this->relation_id)->count();
   }

   public function thumb($version=false)
   {

		// if thumb already exists, return URL
   		
		if( $this->path( $version, true ) )
		{
			return $this->url( $version );
		}
		

		// if not, let's create the new size
		$thumb = config('image.thumb.' . $version);

		if( !isset($thumb) ){ return $this->url(); }

		if( !Storage::exists( $this->path ) )
		{
			return false;
		}
		
		$image = @Image::make( $this->fullPath() )->resize( $thumb['width'] , $thumb['height'], function ($constraint){
				    $constraint->aspectRatio();
				    $constraint->upsize();
				});

	    @$image->save( $this->fullPath( $version ), config('image.quality',100) );

	    // ToDo: move to filesystem


	    return $this->url( $version );

	}

	public function xxs()
	{
		return $this->thumb('xxs');
	}

	public function xs()
	{
		return $this->thumb('xs');
	}

	public function sm()
	{
		return $this->thumb('sm');
	}

	public function md()
	{
		return $this->thumb('md');
	}

	public function lg()
	{
		return $this->thumb('lg');
	}

	public function resizeOriginal()
	{
		return $this->thumb('original');
	}

	public function original()
	{
		return $this->url();
	}

	public function path( $version=false, $exists=false )
	{
		$path = $this->path;	

		if( $version )
		{
			
			$path_version = explode('/' , $path);
			$path_version[ count($path_version)-1 ] = 'th/'.$version.'-'.$path_version[ count($path_version)-1 ];
			$path_version = implode('/', $path_version);			

			
			if( $exists )
			{


				if( Storage::exists($path_version) )
				{
					return $path_version;
				}

				return false;
			}

			return $path_version;
			
		}

		return $path;
	}

	public function fullPath(  $version=false )
	{
		return  public_path() . config('image.upload') . '/' . $this->path( $version );
	}

	public function url( $version=false )
	{
		return url() . config('image.upload') . '/' . $this->path( $version );
	}

	public function getUrlAttribute( $value )
	{
		return str_replace('　','%E3%80%80', $value);
	}
}
