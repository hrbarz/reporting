<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!--link rel="icon" href="../../favicon.ico"-->

    <title>{{ $report->title }}</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">


    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css"> 
      .body {-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none; user-select: none;}
      .forprint { margin-top: 200px; display: none}
      @media print {
          #table_div,#form,.container{
              display: none;
          }
          .forprint { display: block; }
      }

    </style>
  </head>

  <body oncontextmenu="return false" oncopy="return false" oncut="return false" onpaste="return false">

    <div class="forprint"><h2 style="color: #666"> {{ $report->title }}</h2><br><br><br><br><br><img src="/img/mark-3ce.jpg" width="100%"></div>
    <div class="container" style="margin-top: 10px">

   
      <div class="row">
          <div class="col-md-12">
          <img src="http://3comunicaciones.com.ar/images/logoCom.png" width="90px">
          </div>
          <div class="col-md-12">
          <h2 style="color: #666"> {{ $report->title }}</h2>
          </div>
      </div>




    <p>{{ $report->description }}</p>
          <div class="alert alert-warning show-alert" style="display: none" role="alert"></div>

    <div class="row">

    <form action="" id="form" method="get">

      <div class="col-md-2">
      Tipo de medio 
      <select class="form-control selectpicker show-tick" id="medios-select" name="medio">
        <option value="">-</option>
        @foreach($options['medio'] as $key => $value)

        <option value="{{ $value['key'] }}" {!! $_get['medio']==$value['key'] /* || $value['status'] =='enabled'*/ ?'selected="selected"':'' !!}  {{ $value['status'] }}>{{ $value['name'] }}</option>
        @endforeach
      </select>
      </div>


      <div class="col-md-2">
      Localidad
      <select name="localidad" class="form-control selectpicker show-tick">
        <!--option value="">-</option-->
        @foreach($options['localidad'] as $key => $value)
        <option {{ $_get['localidad']==$value['name']?'selected="selected"':'' }} {{ $value['status'] }} >{{$value['name']}}</option>
        @endforeach
      </select>

      </div>
      <div class="col-md-2">
      Sexo
      <select name="sexo" class="form-control selectpicker show-tick">
        <option value="">-</option>
        @foreach($options['sexo'] as $key => $value)
        <option {{ $_get['sexo']==$value['name']?'selected="selected"':'' }} >{{$value['name']}}</option>
        @endforeach
      </select>

      </div>
      
      <div class="col-md-2">
      Edad
      <select name="edad" class="form-control selectpicker show-tick">
        <option value="">-</option>
        @foreach($options['edad'] as $key => $value)
        <option {{ $_get['edad']==$value['name']?'selected="selected"':'' }} >{{$value['name']}}</option>
        @endforeach
      </select>
      </div>

      <div class="col-md-2">
      NSE
      <select name="nse" class="form-control selectpicker show-tick">
        <option value="">-</option>
        @foreach($options['nse'] as $key => $value)
        <option {{ $_get['nse']==$value['name']?'selected="selected"':'' }} >{{$value['name']}}</option>
        @endforeach
      </select>
      </div>

      <div class="col-md-2"><br>
      <button type="button" onclick="visualizar()" class="btn btn-danger btn-block">Visualizar</button>

      </div>

    </div>

    <script >
        function visualizar()
        {
            var mensaje = '';
            var medio = $('select[name="medio"]').val();
            if(medio == 'radio' || medio == 'diario' || medio == 'television' || medio == 'revista' )
            {
                if($('select[name="localidad"]').val() == '')
                {
                   mensaje = 'Seleccione <b>Localidad</b> para proseguir';
                }
            }

            if(medio == '')
            {
                mensaje = 'Seleccione <b>Medio</b> para proseguir';
            }

            if(mensaje != '')
            {
              $('.show-alert').css('display','block').html(mensaje);
            }else{
                $('#form').submit();
            }
        }
    </script>
    <style type="text/css">
      .check-average{
        display: none;
      }
    </style>
    <div class="row">

      <div style="display:none">
        <input type="radio" id="radio_average" name="resultado" value="basic-average" autocomplete="off" {{ $_get['resultado']=='basic-average'?'checked':'' }} >Promedio</label>
      </div>

      <div class="col-md-12 check-no-average" style="{{ $_get['resultado']=='basic-average'?'display:none':'' }}">
        Resultados<br>
       <div class="btn-group " data-toggle="buttons">
       
        <label class="btn btn-default {{ $_get['resultado']=='basic-percent'?'active':'' }} " data-toggle="tooltip" data-placement="bottom" title="Share: participación de contactos con relación al consumo total de ese segmento de medios.">
          <input type="radio" id="radio_default" name="resultado" value="basic-percent" autocomplete="off" {{ $_get['resultado']=='basic-percent'?'checked':'' }} >Ranking general por Share %</label>

        <label class="btn btn-default {{ $_get['resultado']=='basic-numeric'?'active':'' }} check-no-average" data-toggle="tooltip" data-placement="bottom" title="Alcance o Impacto: cantidad de contactos de un medio. Contacto: unidad de medida de consumo de un medio de comunicación en un espacio de tiempo (hora, día, semana, año o fracciones de cada uno).">
          <input type="radio" name="resultado" value="basic-numeric" autocomplete="off" {{ $_get['resultado']=='basic-numeric'?'checked':'' }}>Ranking general por Impacto</label>

        <label class="btn btn-default {{ $_get['resultado']=='extend-percent'?'active':'' }} check-no-average" data-toggle="tooltip" data-placement="bottom" title="Share: participación de contactos con relación al consumo total de ese segmento de medios.">
          <input type="radio" name="resultado" value="extend-percent" autocomplete="off" {{ $_get['resultado']=='extend-percent'?'checked':'' }}>Desagregado por Share %</label>

        <label class="btn btn-default {{ $_get['resultado']=='extend-numeric'?'active':'' }} check-no-average" data-toggle="tooltip" data-placement="bottom" title="Alcance o Impacto: cantidad de contactos de un medio. Contacto: unidad de medida de consumo de un medio de comunicación en un espacio de tiempo (hora, día, semana, año o fracciones de cada uno).">
          <input type="radio" name="resultado" value="extend-numeric" autocomplete="off" {{ $_get['resultado']=='extend-numeric'?'checked':'' }}>Desagregado por Impacto</label>

      </div>
      </div>

     

    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            <h4>Estudio realizado para {{  strtoupper(studly_case($client)) }}</h4>
        </div>
    </div>

    </form>

    <br>

    <div class="row">

      <div class="col-md-12">

      @if(count($data['columns']) > 1)

      <?php 
        if(count($data['columns']) < 3)
        {
            $class = 'col-md-4 col-md-offset-4';
        }
        elseif(count($data['columns']) < 6)
        {
            $class = 'col-md-6 col-md-offset-3';
        }else
        {
            $class = '';

        }
       ?>
      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
       <div id="table_div" class="{{ $class }}" ></div>

        <script type="text/javascript">
            google.charts.load('current', {'packages':['table']});
        google.charts.setOnLoadCallback(drawTable);

      function drawTable() {
        var data = new google.visualization.DataTable();

        @foreach($data['columns'] as $key => $column)
        data.addColumn('{{ $key ==0?'string':'number' }}', '{!!$column!!}');   
        @endforeach
        data.addRows({!! json_encode($data['values']) !!});

        var table = new google.visualization.ChartWrapper({
        'chartType': 'Table',
        'dataTable': data,
        'containerId': 'table_div',
        'options': {
            'width': '100%',
            'showRowNumber' : true
        },
    });
        google.visualization.events.addListener(table, 'ready', function(){
          $(".google-visualization-table-table").attr('class', 'table');
          //$("#table_div").addClass( 'table-bordered table-condensed table-striped center-block' );
        });
        
        table.draw();

        //table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
      }
    </script>

      @else

      <div style="width: 100%; height: 340px; background-image: url('/img/mark-3ce.jpg'); background-size: 50%; background-position: center center; background-repeat: no-repeat; ">
      <div class="col-md-6 col-md-offset-3" style="position: absolute;               /* 2 */
   top: 40%;                         /* 3 */
   transform: translate(0, -50%); color: #D9534F; font-weight: 500">
        <br>
        <br>
        <br>
        @if(Request::input('medio')) 
        <p style="font-size: 18px">
          Si desea conocer más resultados, por favor escribanos a <a href="mailto:servicio@3comunicaciones.com.ar">servicio@3comunicaciones.com.ar</a>. Muchas gracias 
        </p>
        @else
        <p>
          Breve guía de usuario:
          <ol>
              <li>Para iniciar, indique un “tipo de medio” en el primer filtro</li>
              <li>Luego seleccione la “localidad”</li>
              <li>Finalmente elija la categoría de resultado</li>
              <li>También puede sumar a los dos primeros filtros: “sexo”, “edad” y/o “NSE”</li>
          </ol>
        </p>
        @endif
      </div>

      </div>

      @endif
      



      </div>

    </div>

   



<br>
<br>
<br>
      <!-- Site footer -->
      <footer class="footer">
        <p>&copy; {{date('Y')}}, 3CE. Comodoro Rivadavia, Chubut, Argentina.</p>
      </footer>

    </div> <!-- /container -->
<style type="text/css">
  .google-visualization-table-tr-head {
    background-color: #F3F3F3;
  }
  .google-visualization-table-th {
    font-size: 12px;
  }
  .google-visualization-table-td {
    font-size: 12px;
  }
  .google-visualization-table-tr-sel,.google-visualization-table-tr-over{
    background-color: #F5F8D6 !important;
  }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
      $(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $( "#medios-select" ).change(function () {
            var medio_selected = $( "#medios-select option:selected" ).val();
            if(medio_selected == 'redessociales_num'){
              $('.check-no-average').fadeOut('slow')
              $("#radio_average").attr('checked', 'checked');
            }else{
              $('.check-no-average').fadeIn('slow')
              $("#radio_default").attr('checked', 'checked');
            }
        });
      });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<style type="text/css">
  .dropdown-menu>.disabled>a,.dropdown-menu>.disabled>a:hover{
    color:#DCDCDC;
  }

</style>
<script type="text/javascript">
  document.onmousedown=disableclick;
status="Right Click Disabled";
function disableclick(event)
{
  if(event.button==2)
   {
     return false;    
   }
}
</script>
  </body>
</html>
