@include ('admin.partials.header')
@include ('admin.partials.nav')
@include ('admin.partials.alert')

@yield('content')

@include ('admin.partials.footer')
