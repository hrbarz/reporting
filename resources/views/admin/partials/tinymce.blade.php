<script type="text/javascript" src="/js/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
  selector: 'textarea.tinymce',
  height: 500,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor code | divider',
  image_advtab: true,
  valid_children : "+body[style]",
  templates: [
    { title: 'Resumen Ejecutivo', content: '<style> .resumen-ejecutivo{max-width: 965px;}</style><h1 style="font-size: 35px;">Resumen Ejecutivo</h1> <br><br> <table class="resumen-ejecutivo" style="width: 100%;"> <tbody> <tr style="height: 103px;"> <td class="td-circle" style="vertical-align: middle;width: 129px;height: 103px;text-align: center;"> <p class="re-circle" style="display: block;border-radius: 50%;border: 4px solid #d93636"> %</p></td> <td style="">&nbsp;</td><td class="re-block" style="vertical-align: middle;width: 830px;height: 103px;border-left: 4px solid #d93636;padding: 20px;"> <p style="font-size: 18px;"> DESCRIPCION </p> </td> </tr></tbody></table>  <style> @media only screen and (max-width: 767px){.resumen-ejecutivo>tbody>tr td.td-circle{width: 100px !important; height: 100px !important;}.resumen-ejecutivo>tbody>tr p.re-circle{width: 100px !important; height: 100px !important; padding-top: 21% !important; font-size: 34px !important; } } .resumen-ejecutivo>tbody>tr:nth-of-type(odd) p.re-circle { border: 4px solid #949494 !important; color: #949494 !important; } .resumen-ejecutivo>tbody>tr:nth-of-type(odd) td.re-block { border-left: 4px solid #949494 !important; } .resumen-ejecutivo>tbody>tr:nth-of-type(odd) hr.re-line { border-top: 1px solid #949494 !important; } .resumen-ejecutivo>tbody>tr p.re-circle { width: 165px;height: 165px; padding-top: 23%; border: 4px solid #d93636 !important; color: #d93636 !important; font-size: 50px; }.resumen-ejecutivo>tbody>tr td.td-circle { width: 129px;height: 103px; }</style>' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ],
  setup: function(editor) {
      editor.addButton('divider', {
            text: '',
            icon: 'hr',
            onclick: function() {
              tinymce.activeEditor.execCommand('mceInsertContent', false, '<hr class="re-line" style="display:block; width: 38px;border-top: 1px solid #d93636;margin-top: 55%;" />');  
            }
        });
 }});
</script>
<?php return ;?>
tinymce.init({
   selector: "textarea.tinymce",
   theme: "modern",

   content_css: "/admin/css/tinymce.css,/admin/css/bootstrap.css",
   body_class: "tiny-content",

   /* divider */
   toolbar: "styleselect | bold italic underline | removeformat | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | code | image | media | datatable",
   statusbar: false,
   menubar : false,
   convert_fonts_to_spans: true,
   plugins: 'paste,autoresize,link,media,code, fullscreen, image, mention',
   convert_urls: false,

   fullpage_default_doctype: '',

   //paste_as_text: true,
   paste_data_images: false,
   paste_word_valid_elements: "b,strong,i,h2,h3,br,li,ul,blockquote,p",
   extended_valid_elements: "place",
   invalid_elements: "doctype,body,head,meta",
   //paste_auto_cleanup_on_paste : true,
   paste_remove_styles: true,
   paste_remove_styles_if_webkit: true,
   paste_strip_class_attributes: true,
   preview_styles: false,

   object_resizing : false,

   style_formats: [

        {title: 'Title', block: 'h2'},
        {title: 'Subtitle', block: 'h3'},
        {title: 'Blockquote', block: 'blockquote'}
   ],

   setup: function(editor) {

      /*
        editor.addButton('divider', {
            text: '',
            icon: 'hr',
            onclick: function() {
               
              tinymce.activeEditor.execCommand('mceInsertContent', false, "<hr class='divider'>");  

            }
        });
      */
      
        editor.addButton('datatable', {
            text: '',
            icon: 'table',
            onclick: function() {
              
              html = "--- datatable --- <br /> [website] planetyze.com <br /> --- /datatable ---";
              tinymce.activeEditor.execCommand('mceInsertContent', false, html);  

            }
        });


    },

    force_p_newlines : false,

    mentions: {
      delimiter: ['@', '#'],
      queryBy: 'title',
      source: function (query, process, delimiter) {
      // Do your ajax call
      // When using multiple delimiters you can alter the query depending on the delimiter used
          if (delimiter === '@' || delimiter === '#'  ) {
             $.getJSON('/api/search/en.json', function (data) {
                //call process to show the result
                process(data)
             });
          }
      },
      insert: function(item, delimiter) {

          if (delimiter === '@')
          {
              return '<place class="editor-place" data-id="' + item.id + '" data-relation="' + item.relation + '">' + item.title + '</place>&nbsp;';
          }else
          {
              return '[' + item.relation + '=' + item.slug + ']&nbsp;';
          }
      }

    }

 });

function tinyMCE_resize()
{
  setTimeout("tinymce.activeEditor.execCommand('mceAutoResize', 200);",500);
  setTimeout("tinymce.activeEditor.execCommand('mceAutoResize', 200);",1000);
}

function tinyMCE_insert( text )
{
   tinymce.activeEditor.execCommand('mceInsertContent', false, text);
}

function tinyMCE_image( path )
{
  imgTag = '<img src="/upload/'+ path +'" class="img-responsive" />';

  tinymce.activeEditor.execCommand('mceInsertContent', false, imgTag);
  tinyMCE_resize();

}

</script>

<style>
.mce-panel { border:0; border-bottom: 0 solid #f5f5f5; border-top: 0 solid #f5f5f5; }
/* isplay:inline-block;font-family:FontAwesome;font-style:normal;font-weight:normal;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale} */
.mce-i-picture:before{ content:"\f03e"; font-family:FontAwesome; } .mce-i-picture{margin-right: 5px !important; }
.mce-i-video:before{ content:"\f16a"; font-family:FontAwesome; } .mce-i-video {  margin-right: 5px !important; }
.mce-i-map:before{ content:"\f041"; font-family:FontAwesome; padding-left: 2px !important; }
#modal-img .ajax-upload-dragdrop { float:none !important; margin:0 auto !important; }
#modal-img .ajax-file-upload-statusbar { float:none !important; width:100% !important; padding-top: 10px !important; }

#elm1_tblext{
position: fixed;
top:10px;
left:400px;
}
#elm2_tblext{
position: fixed;
top:10px;
left:400px;
}

.cover_picture { background:#FFFDEC; }
.cover_picture .cover_btn { display:none; }

</style>
