<header>

	<nav class="navbar navbar-default navbar-static-top">
	  <div class="container-fluid">

	    <div class="navbar-header">
	      <a class="navbar-brand" href="{{ route('home') }}">
	        <img src="/admin/img/logo.png" />
	      </a>
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-nav">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      
			<ul class="nav navbar-nav navbar-project visible-xs ">
	            <li>
	      			<a href="#" class="btn-menu-left" style="font-size:23px;color:white;margin-left:10px;"><i class="fa fa-chevron-right"></i></a>
		        </li>
	        </ul>
	      
	    </div>

		<div class="collapse navbar-collapse" id="header-nav">

		    <ul class="nav navbar-nav navbar-project hidden-xs">

	            <li>
          			<a href="{{ route('home') }}">{{ config('settings.app.name') }} </a>
		        </li>

	        </ul>

	        <ul class="nav navbar-nav navbar-menu">

	            <li @if( is_route_path('admin.user') ) class="active" @endif><a href="{{ route('admin.users') }}"> <span>Users</span> </a></li>
	            <li @if( is_route_path('admin.reports') ) class="active" @endif><a href="{{ route('admin.reports') }}"> <span>Reports</span> </a></li>
	            
	            <li class="visible-xs"><a href="#"> <span>Logout</span> </a></li>

	        </ul>


	        <ul class="nav navbar-nav navbar-right navbar-last hidden-xs">

	            <li class="dropdown">
	      			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->first_name }} &nbsp; <span class="fa fa-chevron-down"></span></a>

			        <ul class="dropdown-menu" role="menu">
			         <?php /*    <li><a href="{{ route('settings.profile') }}">Profile</a></li> */ ?>
			            <li><a href="{{ route('auth.logout') }}">Logout</a></li>
			        </ul>

		        </li>

	        </ul>





	    </div>






	  </div>
	</nav>

</header>
