<div class="panel panel-default">
  <div class="panel-heading"> 
  	Attach Files

  	@include('admin.partials.file-uploads')


  </div>
  <div class="panel-body">
  
  		<div class="row"> 
  		@foreach($data->media() as $media)

        <div class="col-md-4" style="margin-bottom: 10px;">
  				@include('admin.partials.media')

          {!! Form::select('media_position['.$media->id.']', array_combine(range(1,$media->totalGroup()),range(1,$media->totalGroup())), old('media.position', $media->position), ['class' => 'form-control']) !!} 
  			</div>
  
  		@endforeach
  		</div>

  </div>
</div>