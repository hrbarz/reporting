<div class="alert-container">
@if (count($errors) > 0)

  <div class="container">
  	<div class="alert alert-warning text-center">
  	    <h4><strong>Whoops!</strong> There were some problems with your input.</h4>

  			@foreach ($errors->all() as $error)
  				{{ $error }} <br />
  			@endforeach

  	</div>
  </div>

@endif

@if ( Session::get('alert') )

  <div class="container">
    <div class="alert alert-{{ Session::get('class') }} text-center">
  	    <h3> <i class="fa fa-exclamation-triangle"></i> &nbsp; {{ Session::get('alert') }} </h3>
  	</div>
  </div>

@endif

</div>