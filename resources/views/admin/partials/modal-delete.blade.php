<!-- Modal -->
<div class="modal modal-danger fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
      </div>
      <div class="modal-body">
        	Are you sure you want to delete this <span class="modal-delete-type">item</span>?
					<b>"<span class="modal-delete-item">item</span>"</b>
      </div>
      <div class="modal-footer">
				<div class=" text-center">
					<a type="button" class="btn btn-danger btn-cta">Delete</a>
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
      </div>
    </div>
  </div>
</div>
