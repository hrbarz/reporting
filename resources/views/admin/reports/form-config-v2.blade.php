@extends('admin.layout')

@section('content')


<div class="container">

  <div class="page-header clearfix">

    <div class="row">

        <div class="col-xs-6">
            <h3> Report Config </h3>
        </div>

        <div class="col-xs-6 text-right">

            <a href="{{ route('admin.report.edit', $report->id ) }}" class="btn btn-primary btn-md">Back</a>
            
        </div>

    </div>

  </div>


      

  {!! Form::open(['method' => 'post', 'files' => true]) !!}

	<div class="row">

		<div class="col-md-8">

      <div class="panel panel-default">
        <div class="panel-heading"> Load File </div>
        
        <div class="panel-body">
             <table class="table table-data table-inline">
                <tbody>
                  <tr>
                    <td>  <input type="file" id="files" name="file" class="upload-input"> </td>
                  </tr>
                </tbody>
             </table>
        </div>

        <br>
      <br>


      @if( count(@$type_colums)>0 && count(@$fields_file) > 0 ) 
        <div class="panel-heading"> Config groups </div>
        <div class="panel-body">
              <div class="row">
                <div class="col-md-2">
                   <b>Type</b>
                </div>
                <div class="col-md-4">
                   <b>From</b>
                </div>
                <div class="col-md-4">
                   <b>To</b>
                </div>
                <div class="col-md-2">
                </div>
              </div>
   
              <div class="row block_add_columns" style="padding-top: 10px">
                <div class="col-md-6 col-md-offset-3">
                  <a href="#" class="btn-block btn btn-sm btn-default btn-add-column">Add control columns</a>
                </div>
              </div>
              <div class="row block_insert_default" style="padding-top: 10px">
                <div class="col-md-6 col-md-offset-3">
                  <a href="javascript:insert_groups_default()" class="btn-block btn btn-sm btn-default">Insert groups default</a>
                </div>
              </div>

              <div class="row" style="padding-top: 10px;">
                <div class="col-md-6 col-md-offset-3">             
                  <label><input type="checkbox" name="check_no_group" value="1" class="btn_check_no_group" /> the report does not contain groups</label>
                </div>
              </div>
        </div>

      
 <script type="text/html" id="control_columns">
   <div class="row control_column" style="margin-bottom: 5px;">
      <div class="col-md-2">
         <select class="form-control input-sm group-type" name="process[group][]">
          @foreach(@$type_colums as $group=> $type)
          <option value="{{$group}}">{{$group}}</option>
          @endforeach
         </select>
      </div>
      <div class="col-md-4">
         <select class="form-control input-sm group-from" name="process[from][]">
          @foreach(@$fields_file as $i=> $field)
          <option value="{{$i}}">{{ strip_tags($field) }}</option>
          @endforeach
         </select>
      </div>
      <div class="col-md-4">
         <select class="form-control input-sm group-to" name="process[to][]">
          @foreach(@$fields_file as $i=> $field)
          <option value="{{$i}}">{{ strip_tags($field) }}</option>
          @endforeach
         </select>
      </div>
      <div class="col-md-2">
         <a href="javascript:void(0)" onclick="$(this).parent().parent().remove();" class="btn btn-sm btn-default btn_delete_column"><i class="glyphicon glyphicon-remove"></i></a>
      </div>

    </div>
</script>

      <br>
      <br>
   

      @endif

      </div>
		</div>

		<div class="col-md-4">

      <div class="panel panel-default">
      <div class="panel-heading"> Info </div>
      <div class="panel-body">


          <table class="table table-data">

          <tbody>

            <tr>
              <td width="100"> File </td>
              <td> {{ @$report->config['file'] }} </td>
            </tr>

            <tr>
              <td width="100"> Columns </td>
              <td> {{ count(@$report->config['columns']) }} </td>
            </tr>

            <tr>
              <td width="100"> Processed </td>
              <td> {{ (@$report->config['define_columns']) }} </td>
            </tr>

            </tbody>

            </table>

      </div>

		</div>

	</div>

</div>

  <hr />
  <div class="text-right">
      <button type="button" class="btn btn-default btn-md">Cancel</button>
      <button type="submit" class="btn btn-primary btn-md">Save</button>
  </div>

  {!! Form::close() !!}

</div>





    <script type="text/javascript">
            function insert_groups_default()
            {
                $('.control_column').remove();

                var group_default = {!! config('reports.groups_default') !!};

                for (var i = 0; i < group_default.length ; i++)
                {
                    insert_block_group(group_default[i].name, group_default[i].from ,group_default[i].to);
                }

            }

            function insert_block_group(type, from, to)
            {

               $block_gen = $(tmpl("control_columns", []));
               $block_gen.find('.group-type option[value="' + type + '"]').attr("selected", "selected");
               $block_gen.find('.group-from option').eq(from).attr("selected", "selected");
               $block_gen.find('.group-to option').eq(to).attr("selected", "selected");
               $( ".block_add_columns" ).before( $block_gen );

            }

            $(function() {

              @if(@$report->config['groups'])
                @foreach($report->config['groups'] as $group)
                  insert_block_group('{{$group['name']}}',{{ $group['from'] }} , {{ $group['to'] }});
                @endforeach
              @endif 

            });

            $('.btn-add-column').click(function(event){
                event.preventDefault();
                ///var groups  = { groups : {!! json_encode(@$report->config['groups']) !!}  }
                $( ".block_add_columns" ).before( tmpl("control_columns", []) );
            });

            $('.btn_check_no_group').change(function(){
                $(".block_add_columns").toggle();
            });
         
          </script>

@include('admin.partials.tinymce')
@include('admin.partials.modal-delete')

@stop
