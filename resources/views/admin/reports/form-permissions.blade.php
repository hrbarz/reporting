@extends('admin.layout')

@section('content')


<div class="container">

  <div class="page-header clearfix">

    <div class="row">

        <div class="col-xs-6">
            <h3> Report Permissions </h3>
        </div>

        <div class="col-xs-6 text-right">

            <a href="{{ route('admin.report.edit', $report->id ) }}" class="btn btn-primary btn-md">Back</a>
            
        </div>

    </div>

  </div>

  {!! Form::open(['method' => 'post']) !!}

	<div class="row">

		<div class="col-md-12">

      <div class="panel panel-default">
        
          <div class="bs-example">
            <ul id="tabs-profiles" class="nav nav-tabs">
                
                

                <li id="add-permition" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><spam class="glyphicon glyphicon-plus"></spam> Add permission <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        
                    </ul>
                </li>
            </ul>
            
            <div class="tab-content">
 


            </div>

        </div>


      </div>


      </div>
		</div>

		
</div>

  <hr />
  <div class="text-right">
      <button type="button" class="btn btn-default btn-md">Cancel</button>
      <button type="submit" class="btn btn-primary btn-md">Save</button>
  </div>

  {!! Form::close() !!}

</div>

<script type="text/javascript">
    
  (function(window, document, $, undefined) {
      
      var profilesList = null;
      var permissionsList = null    
      var permissiosActive = null    

      window.Permission = function(profilesList,permissionsList,permissiosActive) {

        this.profilesList = profilesList;
        this.permissionsList = permissionsList;
        this.permissiosActive = permissiosActive;
      }

      Permission.prototype.run = function() {

        createMenuProfiles();
        activeLinks();

      }

      var createMenuProfiles = function() {

        var that = this; 

        $.each( that.profilesList , function( index, value ) {

            var $e = createItemMenu(value.code,value.name);

            var dataProfile = getDataProfile(value.code);

            if(dataProfile !== false)
            {
              newProfile(value.code,value.name,$e,dataProfile);
            }

        });

      }
 
      var getDataProfile = function(code){

        if( this.permissiosActive == null) return false;

        return Object.keys(this.permissiosActive).indexOf(code) >=  0 ? this.permissiosActive[code] : false;

      }

      var createItemMenu = function(code,name){
           $('#add-permition ul').append('<li data-code="'+ code +'" data-name="'+ name +'"><a data-toggle="tab" href="#'+ code +'">'+ name +'</a></li>');

           return $('#add-permition ul').find('[data-code="'+ code +'"]');
      }

      var insertTabProfile = function(code,name){

          $( "#add-permition" ).before( '<li data-code="'+ code +'" data-name="'+ name +'" ><a data-toggle="tab" href="#'+ code +'">'+ name +'</a></li>');                    
      }

      var insertContentProfile = function(code,name, data){

          var checks,content,labels = '';

          if( this.permissionsList !== null) {

         ///   console.log(this.permissionsList);

          $.each(this.permissionsList,function(index,value){
          
          
              var check = '<label class="control-label">'+ index +'</label><br>';

              $.each(value,function(i,val){

                  var checked = false;
                  var attach = '';
                  
                  if( null != data && $.inArray((!$.isNumeric(i) ? i : val),data[index]) >= 0 ){
                    checked = true;
                  }

                  check += '<label class="checkbox-inline"><input type="checkbox" ' + ( checked ? 'checked="checked"' : '' ) + ' name="permissions['+ code +']['+ index +'][]" value="'+ (!$.isNumeric(i) ? i : val) +'"> '+ val +' </label> ';

              });

              check += '<br>';

              checks += check;
              
            });
            labels = checks +' <label class="control-label">Attach</label><br><div class="col-xs-4"><input type="text" class="form-control input-sm" name="permissions['+ code +'][attach]" value="'+ (null != data ? data['attach'] : '') +'"></div>';

          }

          content = '<div id="'+ code +'" data-code="'+ code +'" data-name="'+ name +'" class="tab-pane fade in">  <button type="button" class="btn btn-danger btn-xs delete-profile" style="position: relative;float: right;">delete</button><div class="form-group"> '+ labels + ' <input type="hidden" name="permissions['+ code +'][active]" value="1" /></div></div>';

          $('.tab-content').append(content);

      }

      var deleteProfile = function(code) {


        $('#tabs-profiles').find('[data-code="'+ code +'"]').remove();
        $('.tab-content').find('[data-code="'+ code +'"]').remove();

      }

      var newProfile = function(code,name,$e,data){

          insertTabProfile(code, name);
          insertContentProfile(code, name,data);
            
          $e.remove();
      }

      var activeLinks = function(){

          $( "body" ).on( "click", ".delete-profile", function(e){

              var code = $(this).parent().data('code');
              var name = $(this).parent().data('name');

              deleteProfile(code);
              createItemMenu(code,name);

          } );

          $( "#add-permition" ).on( "click", "li", function(e){

            newProfile($(this).data('code') , $(this).data('name'),$(this),null)

        });

      }

  })(window, document, jQuery);

    var profilesList = {!! $profiles !!};

    var permissionsList = {!! $fields !!};

    var permissiosActive = {!! $permissions !!};


  var behavior = new Permission(profilesList,permissionsList,permissiosActive);
  
  behavior.run();

</script>
    

 


@stop