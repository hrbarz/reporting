<div class="panel panel-default">
  <div class="panel-heading"> Settings </div>
  <div class="panel-body">


  	<table class="table table-data">

		<tbody>

      <tr>
        <td width="100"> Status </td>
        <td> {!! Form::select('status', selectBox(config('settings.status.general')), old('status', $report->status), ['class' => 'form-control']) !!} </td>
      </tr>

      <tr>
        <td width="100"> Type </td>
        <td> {!! Form::select('type', selectBox(config('settings.type')), old('status', $report->type), ['class' => 'form-control']) !!} </td>
      </tr>

      @if( $report->id )
      
      @if( $report->type =='medios')
     

      <tr>
        <td width="100"> Description report </td>
        <td>
            {!! Form::textarea('description', old('description', $report->description) , ['class' => 'form-control', 'style'=> 'height:60px']) !!}
        </td>
      </tr>
      <tr>
        <td width="100"> Config V2 report </td>
        <td>
            <a href="{{ route('admin.report.configv2', $report->id ) }}" type="button" class="btn btn-default btn-block">Open</a>
        </td>
      </tr>
      <tr>
        <td width="100"> Config report </td>
        <td>
            <a href="{{ route('admin.report.config', $report->id ) }}" type="button" class="btn btn-default btn-block">Open</a>
        </td>
      </tr>
      @endif

      @if( ($report->type =='medios' && @$report->config['define_columns'] == 1) 
      || $report->type != '')
			<tr>
				<td width="100"> Permissions </td>
				<td>
            <a href="{{ route('admin.report.permissions', $report->id ) }}" type="button" class="btn btn-default btn-block">Open</a>
        </td>
			</tr>
      @endif
      @endif

		</tbody>

	</table>

  </div>
</div>
