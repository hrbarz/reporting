{!! Form::open(['method' => 'get']) !!}

	<div class="page-header">

      <div class="row">

        <div class="col-xs-12 col-sm-8">


          <div class="form-group page-header-search">
            <input name="search" type="text" class="form-control" style="width:200px" placeholder="Search..." value="{{ $search->filter('search') }}">
            <input type="submit" class="btn btn-primary" value="Search" />
            <a href="#" class="btn btn-default btn-filter"> <i class="fa fa-filter"></i> <span class="hidden-xs">Filter</span> </a>
          </div>


        </div>

        <div class="col-xs-12 col-sm-4 text-right col-xs-left">

            <div class="btn btn-default disabled">Results: <b>{{ $search->total() }}</b> </div>
            <a href="{{ route('admin.report.create') }}" class="btn btn-primary">Create</a>


        </div>

    </div>

	</div>

  <div class="well well-filters" style="display:none">

    <div class="row">

        <div class="col-sm-4 col-lg-3">
          <div class="form-group">
            <label for="exampleInputEmail1">Status</label>

            	{!! Form::select('status', $status, old('status', $search->filter('status')), ['class' => 'form-control']) !!}

          </div>
        </div>

        <div class="col-sm-4 col-lg-3">
          <div class="form-group">
            <label for="exampleInputEmail1">Sort</label>
            {!! Form::select('sort', $sorts, old('sort', $search->filter('sort')), ['class' => 'form-control']) !!}
          </div>
        </div>

    </div>

  </div>

{!! Form::close() !!}
