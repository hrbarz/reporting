@extends('admin.layout')

@section('content')

 <?php // @include ('admin.settings._sidebar') ?>

  <div class="container">

  @include ('admin.reports._search')

  @if( $search->isEmpty() )
    @include('admin.partials.empty')
  @else

    <table class="table table-striped table-bordered table-click">
      <thead>
        <tr>
          <th width="%">TITLE</th>
          <th class="hidden-xs" width="50">STATUS</th>
          <th class="hidden-xs" width="50">TYPE</th>
          <th>DATE</th>
        </tr>
      </thead>
      <tbody>

        @foreach($search->results as $item)
        <tr href="{{ route('admin.report.edit', [$item->id]) }}">
          <td> <b> {{ $item->title }} </b> </td>
          <td class="hidden-xs"><small>{{ $item->status }}</small></td>
          <td class="hidden-xs"><small>{{ $item->type }}</small></td>
          <td> {{ $item->created_at }} </td>
        </tr>
        @endforeach

      </tbody>
    </table>

    {!! $search->pagination() !!}

  @endif

  </div>

@stop
