@extends('admin.layout')

@section('content')


<div class="container">
  {!! Form::open(['method' => 'report', 'files' => true]) !!}

  <div class="page-header clearfix">

    <div class="row">

        <div class="col-xs-6">
            <h3> Report </h3>
        </div>

        <div class="col-xs-6 text-right">
                <button type="submit" class="btn btn-primary btn-md">Save</button>

            @if( $report->id )
            <a href="#" class="btn btn-danger btn-md modal-delete" data-type="page" data-report="{{ $report->title }}" data-target="{{ route('admin.report.delete', $report->id ) }}">Delete</a>
            @endif
            
        </div>

    </div>

  </div>


	<div class="row">

		<div class="col-md-8">

      <div class="panel panel-default">
        <div class="panel-heading"> Content </div>
        <div class="panel-body">

             <table class="table table-data table-inline">

                <tbody>

                  <tr>
                    <td width="100"> title </td>
                    <td>  {!! Form::text('title', old('title', $report->title ) , ['class' => 'form-control', 'style' => 'max-width:100% !important']) !!} </td>
                  </tr>

                  <tr>
                    <td> Resumen </td>
                    <td>   {!! Form::textarea('resumen', old('resumen', $report->resumen) , ['class' => 'form-control tinymce','style'=> 'height:120px']) !!}
                    </td>
                  </tr>

                  <tr>
                    <td> Content </td>
                    <td> 

                      <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">

                          <li role="presentation" class="active" ><a href="#General" aria-controls="General" role="tab" data-toggle="tab">General</a></li>

                         @if(@$fields['localidad'])
                            @foreach($fields['localidad'] as $i => $localidad)
                            <li role="presentation"><a href="#{{ str_slug($localidad) }}" aria-controls="{{ str_slug($localidad) }}" role="tab" data-toggle="tab">{{ $localidad }}</a></li>
                            @endforeach
                          @endif
                        </ul>
<?php
  $content = json_decode($report->content,true);
?>
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active" id="General">{!! Form::textarea('content[General]', old('content[General]', @$content[General]) , ['class' => 'tinymce']) !!} </div>


                         @if(@$fields['localidad'])
                            @foreach($fields['localidad'] as $i => $localidad)
                            <div role="tabpanel" class="tab-pane" id="{{ str_slug($localidad) }}">{!! Form::textarea('content['.$localidad.']', old('content['.$localidad.']', @$content[$localidad]) , ['class' => 'tinymce']) !!} </div>
                             @endforeach
                          @endif

                        </div>

                      </div>

                    </td>
                  </tr>

                </tbody>

              </table>

        </div>

        @if(@$type_colums && $report->type =='medios' )
        <div class="panel-heading"> Factor </div>
        <div class="panel-body">
             <table class="table table-data">
                <tbody>
                  <tr>
                    <td></td>
                  @foreach($type_colums as $type => $val)
                    <td>{{ $type }}</td>
                  @endforeach
                  </tr>
                  
                @if(@$fields['localidad'])
                  @foreach($fields['localidad'] as $localidad)
                  <tr>
                    <td>{{ $localidad }}</td>
                    @foreach($type_colums as $type => $val)
                    <td><input class="form-control input-sm" value="{{ @$factor[$type][$localidad] }}" name="factor[{{ $type }}][{{ $localidad }}]" /></td>
                    @endforeach
                  </tr>
                  @endforeach
                @endif
                  
                </tbody>
             </table>
        </div>
        @endif


        <div class="panel-heading"> Adjunto </div>
        <div class="panel-body">
             <input type="file" id="files" name="filereport" class="upload-input">
             @if($report->filereport !='')
              <a href="{{ config('settings.app.files_cloud').$report->filereport }}" target="_blank">{{ config('settings.app.files_cloud').$report->filereport }}</a>
              <label><input type="checkbox" name="filereportremove" value="1"> Delete File</label>
             @endif
        </div>
        
      </div>


		</div>

		<div class="col-md-4">

       @include ('admin.reports._settings')

		</div>

	</div>

  <hr />
  <div class="text-right">
      <button type="button" class="btn btn-default btn-md">Cancel</button>
      <button type="submit" class="btn btn-primary btn-md">Save</button>
  </div>


</div>
  {!! Form::close() !!}


@include('admin.partials.tinymce')
@include('admin.partials.modal-delete')

@stop
