@extends('admin.layout')

@section('content')

	@include('admin.users._sidebar')

	<div class="container">

		@include('admin.users._header')


		<table class="table table-data">

			<tbody>

				<tr>
					<td width="150" > first name </td>
					<td> {{ $user->first_name }} </td>
				</tr>

				<tr>
					<td > last name </td>
					<td> {{ $user->last_name }} </td>
				</tr>

				<tr>
					<td > email </td>
					<td> {{ $user->email }} </td>
				</tr>

				<tr>
					<td > type </td>
					<td> {{ $user->type }} </td>
				</tr>

				<tr>
					<td > created at </td>
					<td> {{ $user->created_at }} </td>
				</tr>
				<tr>
					<td > status </td>
					<td> {{ $user->status }} </td>
				</tr>

				<tr>
					<td> Login </td>
					<td> <a href="{{ $user->autoLogin() }}" class="btn btn-warning">Login as {{ $user->first_name }}</a> </td>
				</tr>


			</tbody>

		</table>


	</div>

@stop
