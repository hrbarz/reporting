@extends('admin.layout')

@section('content')

  @include('admin.users._sidebar')

  <div class="container">

  @include('admin.users._search')

  @if( $search->isEmpty() )
    @include('admin.partials.empty')
  @else

    <table class="table table-striped table-bordered table-click">
      <thead>
        <tr>
          <th width="%">USER</th>
          <th width="120">TYPE</th>
          <th width="300" class="hidden-xs">EMAIL</th>
          <th width="50">STATUS</th>
          <th width="50" class="hidden-xs hidden-sm">CREATED</th>
        </tr>
      </thead>
      <tbody>

        @foreach($search->results as $user)
        <tr  href="{{ route('admin.user.info', [$user->id]) }}">
          <td> <div class="avatar hidden-xs" > {{ $user->nick }} </div> <b> {{ $user->full_name }} </b> </td>
          <td class="text-center">  <code> {{ $user->type }} </code> </td>
          <td class="hidden-xs"> {{ $user->email }} </td>
          <td><small> {{ $user->status }} </small></td>
          <td class="hidden-xs hidden-sm"> {{ $user->created_at }} </td>
        </tr>
        @endforeach

      </tbody>
    </table>

    {!! $search->pagination() !!}

  @endif

  </div>

@stop
