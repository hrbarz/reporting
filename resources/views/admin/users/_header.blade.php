<div class="page-header clearfix">

	<h3> {{ $user->full_name }} </h3>

</div>

<ul class="nav nav-tabs">
    <li @if( is_route('admin.user.info') ) class="active" @endif><a href="{{ route('admin.user.info', [$user->id]) }}">Info</a></li>
    <li @if( is_route('admin.user.edit') ) class="active" @endif><a href="{{ route('admin.user.edit', [$user->id]) }}">Edit</a></li>
</ul>
