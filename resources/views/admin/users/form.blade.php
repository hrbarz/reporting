@extends('admin.layout')

@section('content')

	@include('admin.users._sidebar')

	<div class="container">

	@if($user->first_name != '')	
		@include('admin.users._header')
	@endif
	
  {!! Form::open(['method' => 'post']) !!}


		<table class="table table-data">

			<tbody>

				<tr>
					<td width="150" > first name </td>
					<td> {!! Form::text('first_name', old('first_name', $user->first_name ) , ['class' => 'form-control', 'style' => 'max-width:100% !important']) !!} </td>
				</tr>

				<tr>
					<td > last name </td>
					<td>  {!! Form::text('last_name', old('last_name', $user->last_name ) , ['class' => 'form-control', 'style' => 'max-width:100% !important']) !!} </td>
				</tr>

				<tr>
					<td > email </td>
					<td> {!! Form::text('email', old('email', $user->email ) , ['class' => 'form-control', 'style' => 'max-width:100% !important']) !!} </td>
				</tr>

				<tr>
					<td > password </td>
					<td> {!! Form::text('password', '' , ['class' => 'form-control', 'style' => 'max-width:100% !important']) !!} </td>
				</tr>

				<tr>
					<td > type </td>
					<td> {!! Form::select('type', selectBox(config('auth.types')), old('type', $user->type), ['class' => 'form-control']) !!} </td>
				</tr>

				<tr>
					<td width="150" > Status </td>
         			<td> {!! Form::select('status', selectBox(config('settings.status.general')), old('status', $user->status), ['class' => 'form-control']) !!} </td>
				</tr>



				<tr>
					<td > created at </td>
					<td> {{ $user->created_at }} </td>
				</tr>
			</tbody>

		</table>
  <button type="submit" class="btn btn-primary btn-md">Save</button>

  {!! Form::close() !!}

	</div>

@stop
