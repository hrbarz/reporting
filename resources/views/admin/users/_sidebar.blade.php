<div id="wrapper">

<!-- Sidebar -->
<div id="sidebar-wrapper">

   @if( isset($search) )

      <h3 class="page-title"> Users </h3>
      <div class="list-group">
        <a href="{{ route('admin.users') . $search->url() }}" class="list-group-item {{ $search->filterClass('type') }}"> <i class="fa fa-user"></i> All contacts </a>

        @foreach( config('auth.types') as $type )
          <a href="{{ route('admin.users',[$type]) . $search->url() }}" class="list-group-item {{ $search->filterClass('type', $type) }}"> <i class="fa fa-circle-o text-info"></i> {{ $type }} </a>
        @endforeach

      </div>

   @else

   <div class="list-group">
     <a href="{{ route('admin.users') }}" class="list-group-item"> <i class="fa fa-chevron-left text-info text-sm"></i> Back </a>
   </div>

   <hr />

   @endif

</div>
<!-- /#sidebar-wrapper -->
