@extends('admin.layout')

@section('content')

  @include('admin.settings._sidebar')

  <div class="container">

    <div class="page-header clearfix">

      <h3> Group </h3>

    </div>

  {!! Form::open(['method' => 'post']) !!}

    <table class="table table-data">

      <tbody>

        <tr>
          <td width="150"> name </td>
          <td> {!! Form::text('name', old('name', $group->name) , ['class' => 'form-control']) !!} </td>
        </tr>

        <tr>
          <td width="150"> status </td>
          <td> {!! Form::select('status', $helper->status(), old('status', $group->status), ['class' => 'form-control']) !!} </td>
        </tr>

      </tbody>

    </table>

  <button type="submit" class="btn btn-primary btn-md">Save</button>
  <a href="{{ route('admin.group.index') }}" class="btn btn-default btn-md">Cancel</a>


  {!! Form::close() !!}

  </div>

@stop
