@extends('admin.layout')

@section('content')

	@include('admin.settings._sidebar')

	<div class="container">


		<div class="page-header">

		  <div class="row">

		    <div class="col-xs-6">
		      <h3> Marketing </h3>
		    </div>

		    <div class="col-xs-6 text-right">
		      <a href="{{ route('admin.marketing.create')}}" class="btn btn-primary btn-md">Create</a>
		    </div>

		  </div>

		</div>

		@if( $search->isEmpty() )
			@include('admin.partials.empty')
		@else

			<table class="table table-striped table-bordered table-click">
			  <thead>
			    <tr>
			      <th width="%">NAME</th>
			      <th width="%">CODE</th>
			      <th width="%">STATUS</th>
			      <th width="150">&nbsp;</th>
			    </tr>
			  </thead>
			  <tbody>

			    @foreach($search->results as $marketing)
			    <tr href="{{ route('admin.marketing.edit', [$marketing->id]) }}">
			      <td> <b> {{ $marketing->name }} </b> </td>
			      <td> <code> {{ $marketing->code }} </code> </td>
			      <td> <b> {{ $marketing->status }} </b> </td>

			      <td>
					  <a class="btn btn-danger btn-xs modal-delete" data-type="marketing" data-item="{{ $marketing->name }}" data-target="{{ route('admin.marketing.delete', [$marketing->id]) }}"> &nbsp; <i class="fa fa-trash"></i> &nbsp; </a>
			          <a href="{{ route('admin.marketing.edit', [$marketing->id]) }}" class="btn btn-warning btn-xs"> edit </a>
			      </td>
			    </tr>
			    @endforeach

			  </tbody>
			</table>


			{!! $search->pagination() !!}

		@endif

	</div>

@stop

@include('admin.partials.modal-delete')
