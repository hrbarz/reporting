
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
            }
    </style>
  </head>

  <body>
 
    <div class="container" style="width: 400px;">
 @if ( !Auth::check() )
      {!! Form::open(['method' => 'post', 'route' => 'auth.login']) !!}

        <h2 class="form-signin-heading">Login</h2>
        
        <label for="inputEmail" class="sr-only">Email</label>
        <input name="email" type="email" value="{{ old('email') }}"class="form-control" placeholder="Email address" 
        required autofocus>
        
        <label for="inputPassword" class="sr-only">Clave</label>
        <input name="password" type="password" class="form-control" placeholder="Password" required>
        

        <div class="checkbox">
          <label>
            <input type="checkbox" name="remember" class="icheck" checked> Recordar
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      {!! Form::close() !!}
  @else
     <a href="/admin/dashboard" class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</a>
  @endif
    </div> <!-- /container -->



  </body>
</html>



<?php /*


                 {!! Form::open(['method' => 'post', 'route' => 'auth.signup']) !!}

                  <label> Nombre </label>
                  <input class="form-control" name="first_name" type="first_name" value="{{ old('first_name') }}">

                  <label> Apellido </label>
                  <input class="form-control" name="last_name" type="last_name" value="{{ old('last_name') }}">

                  <label> Email </label>
                  <input class="form-control" name="email" type="email" value="{{ old('email') }}">

                  <label> Clave </label>
                  <input class="form-control" name="password" type="password">

                  
                  <button type="submit" class="btn btn-block btn-default btn-submit">Registrar</button>

                {!! Form::close() !!} */?>
