<?php

namespace Component\Report\Formatters;

class RowExtraInputFormatter extends InputFormatter
{
    /**
     * Formatea los items pasados por parámetro en función a la configuración.
     *
     * @param $config
     */
    public function format($items, $columns, $config)
    {
        $output = parent::format($items, $columns, $config);

        $keysDefault = $this->getKeysForDefault($config);
        $columnsDefaultsWithIndexCase = $this->getIndexCaseForColumnsDefault($keysDefault);

        foreach ($items as $key => $item) {
            $outputItem = $this->formatValue($item, $config, []);
            $outputItem = $this->formatCheck($item, $config, $outputItem);
            $outputItem = $this->formatGroupMultiCheck($item, $config, $outputItem);
            $outputItem = $this->formatMultiCheck($item, $config, $outputItem);

            $outputItem = $this->formatDefault($item, $keysDefault, $columnsDefaultsWithIndexCase, $outputItem);

            $output[] = $outputItem;
        }

        return $output;
    }

    protected function formatGroupMultiCheck($item, $config, $output)
    {
        $config = $this->getType($config, 'GROUPMULTICHECK');

        $groups = array_reduce($config, function ($acumulator, $itemConfig) use ($item) {
            $group = [];
            $arr = explode('_', $itemConfig['type']);
            $countItemsGroup = $arr[1];

            for ($i = $itemConfig['from']; $i <= $itemConfig['to']; $i = $i + $countItemsGroup) {
                $values = array_slice($item, $i, $countItemsGroup);

                $name = array_shift($values);
                $value = array_filter(array_values($values));


                if (!empty($value) && !is_null($name) && !isNameException($name)) {
                    $element['name'] = $name;
                    $element['value'] = $countItemsGroup == 2 ? $value[0] : array_values($value);

                    $group[] = $element;
                }
            }

            if (!empty($group)) {
                $acumulator[$this->getnameGroup($itemConfig['name'])] = $group;
            }

            return $acumulator;
        }, []);

        return array_merge($output, $groups);
    }

    protected function getType($config, $type)
    {
        return array_filter($config, function ($item) use ($type) {
            $arr = explode('_', $item['type']);
            return $item['type'] == $type || (is_array($arr) ? $arr[0] == $type : false);
        });
    }
}