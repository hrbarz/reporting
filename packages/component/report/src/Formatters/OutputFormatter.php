<?php
/**
 * Created by TreeByte.
 * User: Luis Barzola
 */

namespace Component\Report\Formatters;


class OutputFormatter
{
    public function tableFormat($cursor, $mediaType, $tableType)
    {
        $items = $this->filterByMediaType($cursor, $mediaType, $tableType['data']['type']);

        return $this->filterByTableType($items, $tableType);

    }
    
    protected function filterByMediaType($cursor, $mediaType, $dataType)
    {
        $elements = [];
        foreach ($cursor as $item) {
            foreach ($item[$mediaType] as $content){
                $key = $this->getKeyForFilter($content);
                $value = $this->getValueForFilter($content, $dataType);

                $elements[$key] = isset($elements[$key]) ?
                    array_merge($elements[$key], $value) :
                    $value;
            }
        }

        return $elements;
    }

    protected function getKeyForFilter($content)
    {
        if (isset($content['name'])) {
            return $content['name'];
        }

        return $content;
    }

    protected function getValueForFilter($content, $dataType)
    {
        if (isset($content['value'])) {
            if (is_array($content['value'])) {
                return $content['value'];
            }

            return [$content['value']];
        }

        $value = $dataType == 'percent' ? '%' : 'Cantidad';
        return [$value];
    }
    
    protected function filterByTableType($items, $tableType)
    {
        $tableFormatter = new TableFormatter();

        $method = $tableType['type'].$tableType['data']['type'];

        $config = [];
        $config['columns'] = isset($tableType['columns']) ?
            $tableType['columns'] : null;

        $config['extendValue'] = isset($tableType['data']['extendValue']) ?
            $tableType['data']['extendValue'] : null;

        $config['numberOfRespondents'] = isset($tableType['numberOfRespondents']) ?
            $tableType['numberOfRespondents'] : 0;

        return $tableFormatter->$method($items, $config);

    }



    /**
     * @param \MongoDB\Driver\Cursor $cursor
     */
    public function existsValuesFormat($cursor, $columns)
    {
        $output = [];

        foreach ($cursor as $item) {
            foreach ($columns as $column) {
                $previous = isset($output[$column]) ? $output[$column] : array();
                if (!in_array($item[$column], $previous) && $item[$column] != '' ) {
                    $output[$column] = array_merge($previous, array($item[$column]));
                }
            }
        }

        foreach ($columns as $column) {
            if (!isset($output[$column]) || !is_array($output[$column])) continue;

            asort($output[$column]);
            $output[$column] = array_values($output[$column]);
        }

        return $output;
    }
}