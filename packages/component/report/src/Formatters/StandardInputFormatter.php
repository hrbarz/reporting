<?php

namespace Component\Report\Formatters;

class StandardInputFormatter extends InputFormatter
{
    /**
     * Formatea los items pasados por parámetro en función a la configuración.
     *
     * @param $config
     */
    public function format($items, $columns, $config)
    {
        $output = parent::format($items, $columns, $config);

        $keysDefault = $this->getKeysForDefault($config);
        $columnsDefaultsWithIndexCase = $this->getIndexCaseForColumnsDefault($keysDefault);

        foreach ($items as $item) {
            $outputItem = $this->formatValue($item, $config, []);
            $outputItem = $this->formatCheck($item, $config, $outputItem);
            $outputItem = $this->formatMultiCheck($item, $config, $outputItem);

            $outputItem = $this->formatDefault($item, $keysDefault, $columnsDefaultsWithIndexCase, $outputItem);

            $output[] = $outputItem;
        }

        return $output;
    }

    protected function formatMultiCheck($item, $config, $output)
    {
        foreach ($config as $itemConfig) {
            if ($itemConfig['type'] == 'MULTICHECK') {

                $from = $itemConfig['from'];

                $range = $itemConfig['to'] - $itemConfig['from'] + 1;

                $value = array_slice($item, $from, $range);
                $value = array_filter(array_values($value));

                if (!empty($value)) {
                    $element = array();
                    $element['name'] = $this->getName($this->columns[$itemConfig['from']]);
                    $element['value'] = array_values($value);

                    $output[$this->getnameGroup($itemConfig['name'])][] = $element;
                }
            }
        }

        return $output;
    }
}