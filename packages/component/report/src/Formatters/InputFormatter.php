<?php
/**
 * Devuelve el reporte en función al formato especificado.
 */

namespace Component\Report\Formatters;

use Illuminate\Support\Collection;
use Component\Report\Formatters\Validators\InputFormatterValidator;

class InputFormatter
{
    /**
     * @var InputFormatterValidator
     */
    protected $validator = null;

    /**
     * @var array
     */
    protected $columns = null;

    /**
     * ReportFormatter constructor.
     *
     * @param $reader
     *
     * @return void
     */
    public function __construct()
    {
        $this->validator = new InputFormatterValidator();
    }

    /**
     * Formatea los items pasados por parámetro en función a la configuración.
     *
     * @param $config
     */
    public function format($items, $columns, $config)
    {
        $this->validator->validateConfig($config);

        $this->columns = $columns;

        return [];
    }

    /**
     * Agrega a la salida los items que corresponde con al tipo VALUES
     *
     * @param array $output Array de salida, se le agregan los items formateados.
     * @param array $config Contiene las características de los items.
     *
     * @return array
     */
    protected function formatValue($item, $config, $output)
    {
        foreach ($config as $itemConfig) {
            if ($itemConfig['type'] == 'VALUE') {
                for ($i = $itemConfig['from']; $i <= $itemConfig['to']; $i++){

                    if(is_null($item[$this->columns[$i]]) || $item[$this->columns[$i]] === '') continue;

                    $element['name'] = $this->columns[$i];
                    $element['value'] = $item[$this->columns[$i]];

                    $output[$this->getnameGroup($itemConfig['name'])][] = $element;
                }
            }
        }

        return $output;
    }

    /**
     * Agrega a la salida los items que corresponde con al tipo CHECK
     *
     * @param array $item Contiene los items a formatear
     * @param array $config Configuración de los tipos de cada columna
     * @param array $output Elementos ya formateados.
     *
     * @return array
     */
    protected function formatCheck($item, $config, $output)
    {
        foreach ($config as $itemConfig) {
            if ($itemConfig['type'] == 'CHECK') {

                $from = $itemConfig['from'];
                $range = $itemConfig['to'] - $itemConfig['from'] + 1;

                $value = array_slice($item, $from, $range);
                $value = array_filter(array_values($value));

                if (!isset($output[$this->getnameGroup($itemConfig['name'])])) {
                    $output[$this->getnameGroup($itemConfig['name'])] = array();
                }

                $output[$this->getnameGroup($itemConfig['name'])] = array_merge(
                    $output[$this->getnameGroup($itemConfig['name'])], $value
                );
            }
        }

        return $output;
    }

    /**
     * Agrega a la salida los items que corresponde con al tipo MULTICHECK
     *
     * @param array $item Contiene los items a formatear
     * @param array $config Configuración de los tipos de cada columna
     * @param array $output Elementos ya formateados.
     *
     * @return array
     */
    protected function formatMultiCheck($item, $config, $output)
    {
        foreach ($config as $itemConfig) {
            if ($itemConfig['type'] == 'MULTICHECK') {

                $from = $itemConfig['from'];

                $range = $itemConfig['to'] - $itemConfig['from'] + 1;

                $value = array_slice($item, $from, $range);
                $value = array_filter(array_values($value));

                if (!empty($value)) {
                    $element = array();
                    $element['name'] = $this->getName($this->columns[$itemConfig['from']]);
                    $element['value'] = array_values($value);

                    $output[$this->getnameGroup($itemConfig['name'])][] = $element;
                }
            }
        }

        return $output;
    }

    /**
     * Agrega a la salida los items que no corresponden a ningún tipo.
     *
     * @param array $item Contiene los items a formatear
     * @param array $config Configuración de los tipos de cada columna
     * @param array $output Elementos ya formateados.
     *
     * @return array
     */
    protected function formatDefault($item, $keys, $columnsDefaultsWithIndexCase, $output)
    {
        foreach ($keys as $i) {
            $columnName = $this->getName($this->columns[$i]);
            $value = trim($item[$this->columns[$i]]);

            $columnIndexCase = $columnsDefaultsWithIndexCase[$this->columns[$i]];

            $output[$columnIndexCase] = $columnName == $value ?
                true : (is_float($value) ? (int) $value : $value);
            
            $output['columns'][$columnIndexCase] = $columnName;
        }

        return $output;
    }

    protected function getKeysForDefault($config)
    {
        $range = array();
        foreach ($config as $itemConfig) {
            $range = array_merge(
                $range, range($itemConfig['from'], $itemConfig['to'])
            );
        }

        $total = range(0, count($this->columns)-1);

        return array_diff($total, $range);
    }

    protected function getIndexCaseForColumnsDefault($keys)
    {
        $outputColumns = [];
        foreach ($keys as $key) {
            $outputColumns[$this->columns[$key]] = index_case($this->getName($this->columns[$key]));
        }

        return $outputColumns;
    }

    protected function getName($columnName)
    {
        $arr = explode('-',strip_tags($columnName));
        return trim($arr[0]);
    }

    protected function getNameGroup($name)
    {
        return 'group_'.$name;
    }

}