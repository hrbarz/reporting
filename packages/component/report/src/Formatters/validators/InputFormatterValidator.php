<?php
/**
 * Created by TreeByte.
 * User: Luis Barzola
 */

namespace Component\Report\Formatters\Validators;


class InputFormatterValidator
{
    /**
     * Valida que config esté bien formado.
     *
     * @param $config
     */
    public function validateConfig($config)
    {
        foreach ($config as $item) {
            $this->validateItem($item);
        }
    }

    protected function validateItem($item)
    {
        $properties = array('name', 'type', 'from', 'to');

        if (count(array_intersect(array_keys($item), $properties)) !== 4) {
            throw new \Exception(
                'Se encontró un error en '. $this->arrayDisplay($item) .
                '.Se espera que cada elemento tenga las siguientes propiedades: ' .
                implode(', ', $properties)
            );
        }

        $types = array('MULTICHECK', 'VALUE', 'CHECK', 'GROUPMULTICHECK_2', 'GROUPMULTICHECK_17');

        if (!in_array($item['type'], $types)) {
            throw new \Exception(
                'Se encontró un error en '. $this->arrayDisplay($item) .
                '. Se espera los tipos: ' . implode(', ', $types)
            );
        }

        if (!(is_numeric($item['from']) && is_numeric($item['to']))) {
            throw new \Exception(
                'Se encontró un error en '. $this->arrayDisplay($item) .
                '. Los valores de from y to deben ser numéricos'
            );
        }
    }

    function arrayDisplay($input)
    {
        return implode(
            ', ',
            array_map(
                function ($v, $k) {
                    return sprintf("%s => '%s'", $k, $v);
                },
                $input,
                array_keys($input)
            )
        );
    }
}