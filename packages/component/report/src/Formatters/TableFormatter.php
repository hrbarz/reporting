<?php
/**
 * Contiene las funciones de filtro de tablas.
 */

namespace Component\Report\Formatters;


class TableFormatter
{
    public function basicNumeric($items, $config)
    {
        $column = 'Cantidad';

        $items = $this->getCountElement($items);

        $output = [];
        foreach ($items as $mediaName => $count) {
            $output[$mediaName][$column] = $count;
        }

        return $this->extendValues($output, $config['extendValue']);
    }

    public function basicPercent($items, $config)
    {
        $column = 'Porcentaje';

        $items = $this->getCountElement($items);

        $total = 0;
        foreach ($items as $mediaName => $count) {
            $items[$mediaName] = $count;
            $total += $count;
        }

        $output = [];
        foreach ($items as $media => $count) {
            $value = $count == 0 ? $count : round(($count / $total) * 100, 1);
            $output[$media][$column] = $value;
        }

        return $output;
    }

    public function basicAverage($items, $config)
    {
        $column = 'Promedio';

        $averages = [];
        foreach ($items as $mediaName => $values) {
            $averages[$mediaName] = count($values) === 0 ?
                0 : round(array_sum($values) / count($values), 1);
        }

        arsort($averages);

        foreach ($averages as $mediaName => $value) {
            $output[$mediaName][$column] = $value;
        }

        return $output;
    }

    public function extendNumeric($items, $config)
    {
        // obtengo las columnas
        $columns = $this->getColumns($items, $config['columns']);

        $columnsTemplate = $this->getColumnsTemplate($items, $columns);

        // obtengo los valores en números
        $output = [];
        foreach ($items as $media => $elements) {
            $output[$media] = $columnsTemplate;
            foreach ($elements as $element) {
                if (isset($columns[$element])) {
                    $output[$media][$columns[$element]] += 1;
                }
            }
        }

        ksort($output);

        return $this->extendValues($output, $config['extendValue']);
    }

    public function extendPercent($items, $config)
    {
        // obtengo las columnas
        $columns = $this->getColumns($items, $config['columns']);

        $columnsTemplate = $this->getColumnsTemplate($items, $columns);

        // obtengo los valores en números
        $total = $columnsTemplate;
        foreach ($items as $media => $elements) {
            $items[$media] = $columnsTemplate;
            foreach ($elements as $element) {
                if (isset($columns[$element])) {
                    $items[$media][$columns[$element]] += 1;
                    $total[$columns[$element]] += 1;
                }
            }
        }

        // transformo los valores a porcentaje
        foreach ($items as $media => $elements) {
            foreach ($elements as $type => $count) {
                $value = $total[$type] == 0 ? $total[$type] : round(($count / $total[$type]) * 100, 1);
                $items[$media][$type] = $value;
            }

        }

        ksort($items);

        return $items;
    }

    protected function getColumns($items, $columns)
    {
        if (is_null($columns)) {
            foreach ($items as $elements) {
                foreach ($elements as $element) {
                    $columns[$element] = $element;
                }
            }
        }

        return $columns;
    }

    protected function getColumnsTemplate($items, $columns)
    {
        $columnsTemplate = [];

        if (is_null($columns)) {
            foreach ($items as $elements) {
                foreach ($elements as $element) {
                    $columnsTemplate[$element] = 0;
                }
            }
        } else {
            foreach ($columns as $key => $column) {
                $columnsTemplate[$column] = 0;
            }
        }

        return $columnsTemplate;
    }

    protected function extendValues($items, $extendValue)
    {
        if (is_null($extendValue)) {
            return $items;
        }

        foreach ($items as $key => $item) {
            if (is_array($item)) {
                foreach ($item as $column => $value) {
                    $items[$key][$column] = $value * $extendValue;
                }
            } else {
                $items[$key] = $item * $extendValue;
            }
        }

        return $items;
    }

    protected function getCountElement($items)
    {
        foreach ($items as $key => $item) {
            $items[$key] = count($item);
        }

        arsort($items);

        return $items;
    }

}