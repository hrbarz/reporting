<?php

namespace Component\Report;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{



    public function index()
    {
        $builder = new ReportFormatterBuilder();

        $reportFormatter = $builder->build('BBDD_3CE_EM_DIGITAL.xlsx');

        $columns = $reportFormatter->getColumns();

        $formatted = $reportFormatter->format($this->getFormatterConfig());

        dd($formatted);
    }

    protected function getFormatterConfig()
    {
        return [
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 14, 'to' => 29],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 30, 'to' => 45],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 46, 'to' => 61],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 62, 'to' => 77],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 78, 'to' => 93],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 94, 'to' => 109],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 110, 'to' => 125],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 126, 'to' => 141],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 142, 'to' => 157],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 158, 'to' => 173],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 174, 'to' => 189],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 190, 'to' => 205],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 206, 'to' => 221],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 222, 'to' => 237],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 238, 'to' => 253],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 254, 'to' => 269],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 270, 'to' => 285],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 286, 'to' => 301],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 302, 'to' => 317],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 318, 'to' => 333],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 334, 'to' => 349],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 350, 'to' => 365],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 366, 'to' => 381],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 382, 'to' => 397],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 398, 'to' => 413],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 414, 'to' => 429],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 430, 'to' => 461],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 462, 'to' => 477],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 478, 'to' => 493],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 494, 'to' => 509],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 510, 'to' => 525],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 526, 'to' => 541],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 542, 'to' => 557],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 558, 'to' => 573],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 574, 'to' => 589],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 590, 'to' => 605],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 606, 'to' => 621],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 622, 'to' => 637],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 638, 'to' => 653],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 654, 'to' => 669],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 670, 'to' => 685],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 686, 'to' => 701],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 702, 'to' => 717],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 718, 'to' => 733],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 734, 'to' => 749],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 750, 'to' => 765],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 766, 'to' => 781],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 782, 'to' => 797],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 798, 'to' => 813],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 814, 'to' => 829],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 830, 'to' => 845],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 846, 'to' => 861],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 862, 'to' => 877],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 878, 'to' => 893],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 894, 'to' => 909],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 910, 'to' => 925],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 926, 'to' => 941],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 942, 'to' => 957],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 958, 'to' => 973],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 974, 'to' => 989],
            ['name' => 'radio', 'type' => 'MULTICHECK', 'from' => 990, 'to' => 1005],

            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1006, 'to' => 1019],
            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1020, 'to' => 1026],
            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1027, 'to' => 1033],
            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1034, 'to' => 1040],
            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1041, 'to' => 1047],
            ['name' => 'diario', 'type' => 'MULTICHECK', 'from' => 1048, 'to' => 1054],

            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1064, 'to' => 1079],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1080, 'to' => 1095],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1096, 'to' => 1111],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1112, 'to' => 1127],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1128, 'to' => 1143],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1144, 'to' => 1159],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1160, 'to' => 1175],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1176, 'to' => 1191],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1192, 'to' => 1207],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1208, 'to' => 1223],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1224, 'to' => 1239],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1240, 'to' => 1255],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1256, 'to' => 1287],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1288, 'to' => 1303],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1304, 'to' => 1319],
            ['name' => 'television', 'type' => 'MULTICHECK', 'from' => 1320, 'to' => 1335],

            ['name' => 'diarioonline', 'type' => 'VALUE', 'from' => 1336, 'to' => 1358],

            ['name' => 'redessociales', 'type' => 'CHECK', 'from' => 1359, 'to' => 1377],


        ];
    }
}
