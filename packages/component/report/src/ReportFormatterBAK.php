<?php
/**
 * Devuelve el reporte en función al formato especificado.
 */

namespace Component\Report;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Component\Report\ReportFormatterValidator;
use Maatwebsite\Excel\Readers\LaravelExcelReader;

class ReportFormatter
{
    /**
     * @var LaravelExcelReader
     */
    protected $reader = null;

    /**
     * @var ReportFormatterValidator
     */
    protected $validator = null;

    /**
     * @var Collection
     */
    protected $items = null;

    /**
     * @var array
     */
    protected $columns = null;

    /**
     * ReportFormatter constructor.
     *
     * @param $reader
     *
     * @return void
     */
    public function __construct($items, $columns, $validator)
    {
        $this->items = $items;
        $this->validator = $validator;
        $this->columns = $columns;
    }

    /**
     * Devuelve las columnas del documento.
     *
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Export el documento
     *
     * @param $config
     */
    public function format($config)
    {
        $this->validator->validateConfig($config);

        $output = array();

        foreach ($this->items as $key => $item) {
            $outputItem = array();
            $outputItem = $this->formatValue($item, $config, $outputItem);
            $outputItem = $this->formatCheck($item, $config, $outputItem);
            $outputItem = $this->formatMultiCheck($item, $config, $outputItem);

            $outputItem = $this->formatDefault($item, $config, $outputItem);

            $output[] = $outputItem;
        }

        return $output;
    }

    /**
     * Agrega a la salida los items que corresponde con al tipo VALUES
     *
     * @param array $output Array de salida, se le agregan los items formateados.
     * @param array $config Contiene las características de los items.
     *
     * @return array
     */
    protected function formatValue($item, $config, $output)
    {
        foreach ($config as $itemConfig) {
            if ($itemConfig['type'] == 'VALUE') {
                for ($i = $itemConfig['from']; $i <= $itemConfig['to']; $i++){

                    if($item[$this->columns[$i]] == '') continue;

                    $element = array();
                    $element['name'] = $this->columns[$i];
                    $element['value'] = $item[$this->columns[$i]];

                    $output[$this->getnameGroup($itemConfig['name'])][] = $element;
                }
            }
        }

        return $output;
    }

    protected function formatCheck($item, $config, $output)
    {
        foreach ($config as $itemConfig) {
            if ($itemConfig['type'] == 'CHECK') {

                $from = $itemConfig['from'];
                $range = $itemConfig['to'] - $itemConfig['from'] + 1;

                $value = array_slice($item, $from, $range);
                $value = array_filter(array_values($value));

                if (!isset($output[$this->getnameGroup($itemConfig['name'])])) {
                    $output[$this->getnameGroup($itemConfig['name'])] = array();
                }

                $output[$this->getnameGroup($itemConfig['name'])] = array_merge($output[$this->getnameGroup($itemConfig['name'])], $value);

            }
        }

        return $output;
    }

    protected function formatMultiCheck($item, $config, $output)
    {
        foreach ($config as $itemConfig) {
            if ($itemConfig['type'] == 'MULTICHECK') {

                $from = $itemConfig['from'];
                $range = $itemConfig['to'] - $itemConfig['from'] + 1;

                $value = array_slice($item, $from, $range);
                $value = array_filter(array_values($value));

                if (!empty($value)) {
                    $element = array();
                    $element['name'] = $this->getName($this->columns[$itemConfig['from']]);
                    $element['value'] = array_values($value);

                    $output[$this->getnameGroup($itemConfig['name'])][] = $element;
                }
            }
        }

        return $output;
    }

    protected function formatDefault($item, $config, $output)
    {
        $range = array();
        foreach ($config as $itemConfig) {
            $range = array_merge(
                $range, range($itemConfig['from'], $itemConfig['to'])
            );
        }

        $total = range(0, count($this->columns)-1);

        $diff = array_diff($total, $range);

        foreach ($diff as $i) {
            $columnName = $this->getName($this->columns[$i]);
            $value = trim($item[$this->columns[$i]]);

            $output[index_case($columnName)] = $columnName == $value ? true : (is_float($value) ? (int) $value : $value);
            
            $output['columns'][index_case($columnName)] = $columnName;
        }

        return $output;
    }

    protected function getName($columnName)
    {
        $arr = explode('-',strip_tags($columnName));
        return trim($arr[0]);
    }

    protected function getNameGroup($name)
    {
        return 'group_'.$name;
    }

}