<?php
/**
 * Created by TreeByte.
 * User: Luis Barzola
 */

namespace Component\Report\Repositories;

use MongoDB\Client;


class DatabaseManager
{
    protected static $instance = null;

    private function __construct(){}

    /**
     * Devuelve una instancia de DatabaseManager
     *
     * @return DatabaseManager
     */
    public static function getInstance($config, $driverOptions)
    {
        if (is_null(self::$instance)) {
            self::$instance = (new Client(
                'mongodb://'.$config['host'].':'.$config['port']
                ,[],
                $driverOptions)
            )->{$config['database']};
        }

        return self::$instance;
    }
}