<?php
/**
 * Capa de acceso a la base de datos.
 */

namespace Component\Report\Repositories;

use Component\Report\Repositories\DatabaseManager;

class Repository
{
    /**
     * Nombre de colección
     *
     * @var null
     */
    protected $collectionName = null;

    /**
     * Manager de la base de datos.
     *
     * @var DatabaseManager
     */
    protected $dbm = null;

    /**
     * Repository constructor.
     *
     * @param string $collectionName Nombre de la colección.
     */
    public function __construct($collectionName)
    {
        $this->collectionName = $collectionName;

        $this->dbm = $this->getDBM();
    }

    /**
     * Devuelve una instancia de DatabaseManager
     *
     * @return mixed
     */
    protected function getDBM()
    {
        $config = [
            'host' => config('database.connections.mongodb.host'),
            'port' => config('database.connections.mongodb.port'),
            'database' => config('database.connections.mongodb.database'),
        ];

        $driverOptions= [
            'typeMap' => [
                'root' => 'array',
                'document' => 'array',
                'array' => 'array'
            ]
        ];


        return DatabaseManager::getInstance($config, $driverOptions);
    }

    /**
     * Inserta el datos en la colección
     *
     * @param $data
     * @param string $collectionName Nombre de la colección, es opcional permite cambiar de nombre de colección.
     *  En caso de no definir se usa el de defecto
     */
    public function insert($data, $collectionName = null)
    {
        $this->instanceCollection($collectionName)->insertMany($data);
    }

    /**
     * @param $where
     * @param null $collectionName
     * @return array
     */
    public function findOne($where, $collectionName = null)
    {
        return $this->instanceCollection($collectionName)->findOne(
            $where
        );
    }

    public function drop($options = [], $collectionName=null) {
        return $this->instanceCollection($collectionName)->drop($options);
    }

    public function count($options = [], $collectionName=null)
    {
        return $this->instanceCollection($collectionName)->count($options);
    }


    public function find($where = array(), $options = array(), $collectionName = null)
    {
        $document =  $this->instanceCollection($collectionName)
            ->find($where, $options);

        return $document;
    }

    /**
     * Devuelve el nombre de la colección
     *
     * @param $collectionName
     *
     * @return \MongoDB\Collection
     */
    protected function instanceCollection($collectionName)
    {
        if (is_null($collectionName)) {
            $collectionName = $this->collectionName;
        }

        return $this->dbm->$collectionName;
    }

    public function getCollection()
    {
        return $this->collectionName;
    }
}