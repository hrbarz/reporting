<?php
/**
 * Interfaz para guardar y aplicar filtros sobre un reporte.
 */

namespace Component\Report;

use Component\Report\Formatters\InputFormatter;
use Component\Report\Formatters\OutputFormatter;
use Component\Report\Repositories\Repository;
use Maatwebsite\Excel\Readers\LaravelExcelReader;

class Report
{
    /**
     * Contiene una instancia de Repository
     *
     * @var Repository
     */
    protected $repository = null;

    /**
     * Instancia de LaravelExcelReader
     *
     * @var LaravelExcelReader
     */
    protected $excelReader = null;

    /**
     * Intancia de InputFormatter
     * @var InputFormatter
     */
    protected $inputFormatter = null;

    protected $versionParse = null;
    protected $items = null;
    protected $columns = null;
    protected $collectionName = null;

    /**
     * Report constructor.
     *
     * @param $repository
     * @param $excelReader
     *
     * @return Report
     */
    public function __construct($repository, $excelReader, $versionParse, $inputFormatter)
    {
        $this->repository = $repository;
        $this->excelReader = $excelReader;
        $this->inputFormatter = $inputFormatter;
        $this->versionParse = $versionParse;

        $this->collectionName =  $this->repository->getCollection();
    }

    /**
     * Guarda el reporte a partir de la configuración enviada.
     *
     * @param $config
     *
     * @return void
     */
    public function saveReport($config)
    {
        if (!isset($this->items)) {
            $this->loadItems();
        }

        $formatted = $this->inputFormatter->format(
            $this->items, $this->columns, $config
        );

        $this->repository->drop();
        $this->repository->insert($formatted);

        $this->repository->drop([], $this->collectionName . '_info');
        $this->repository->insert(
            array($this->columns), $this->collectionName . '_info'
        );
    }

    /**
     * Devuelve las columnas del documento.
     *
     * @return array
     */
    public function getColumns($isNewDocument = false)
    {
        $document = $this->repository->findOne(
            array(), $this->collectionName . '_info'
        );

        // sino lo encontró lo busca en el documento
        if (!isset($document) || $isNewDocument) {
            $this->loadItems();
            $document = $this->columns;
        }

        unset($document['_id']);

        return $document;

    }

    public function count()
    {
        return $this->repository->count([], $this->collectionName);
    }

    protected function loadItems()
    {
        $this->items = $this->excelReader->all()->toArray();
        $this->columns = array_keys($this->items[0]);
    }

    /**
     * Devuelve los valores existentes para las columnas enviadas.
     *
     * @param array $columns Columnas
     *
     * @return array
     */
    public function getExistsValues(array $columns)
    {

        $where = [];
        $projection['_id'] = 0;

        foreach ($columns as $column) {
            $where[$column] =  ['$exists' => true];
            $projection[$column] = 1;
        }

        $options = [
            'projection' => $projection
        ];

        $cursor =  $this->repository->find($where, $options);

        $outputFormatter = new OutputFormatter();

        return $outputFormatter->existsValuesFormat($cursor, $columns);


        /*
            ["group_revista" => ['$exists' => true]],
            ['projection' => ['_id' => 0,'group_revista' => 1,]]

         */
    }

    /**
     * Devuelve una tabla que cumple con la propiedad enviada.
     *
     * @param array $properties Contiene la configuración que se aplicará para
     *  buscar y devolver los datos
     *
     * @return array
     */
    public function getTable($properties)
    {
        $options = [
            'projection' => ['_id' => 0, $properties['mediaType'] => 1]
        ];

        $where = $properties['filters'];
        $where[$properties['mediaType']] = ['$exists' => true];

        $cursor =  $this->repository->find($where, $options);

        $items = $cursor->toArray();
        if (count($items) != 0) {
            $outputFormatter = new OutputFormatter();
            return $outputFormatter->tableFormat(
                $items, $properties['mediaType'], $properties['view']
            );
        }

        return [];

    }
}