<?php

namespace Component\Report;

use Illuminate\Support\ServiceProvider;

class ReportServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->router->group(['namespace' => 'SmithFoto\AdminZ\Http\Controllers'],
            function(){
                require __DIR__.'/Http/routes.php';
            });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Component\Report\ReportController');
    }
}
