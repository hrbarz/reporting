<?php
/**
 * Crea una instancia de Report
 */

namespace Component\Report;

use Component\Report\Formatters\InputFormatter;
use Component\Report\Formatters\RowExtraInputFormatter;
use Component\Report\Formatters\StandardInputFormatter;
use Illuminate\Support\Facades\Config;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Readers\LaravelExcelReader;
use Component\Report\Repositories\Repository;

class ReportBuilder
{
    const VERSION_STANDARD = 1;
    const VERSION_MULTI_GROUP = 2;

    /**
     * @var array
     */
    protected $items = null;

    protected $columns = null;

    /**
     * Crea una instancia de Report
     *
     * En caso de existir la colección se pisa y se envía $filePath. Además
     *  ejecutarse saveReport, se pisará el existente.
     * En caso de existir la colección y no se envió $filePath, se usa el
     *  existente.
     * Sino existe la colección y se envió $filePath, se crear la colección con
     *  el nombre enviado.
     *
     * @param string $reportNameCollection Nombre de la colección.
     * @param string $filePath Ruta donde se encuentra el archivo.
     *
     * @return Report
     */
    public function build($reportNameCollection, $filePath = null, $versionParse = self::VERSION_STANDARD)
    {
        ini_set('memory_limit', -1);

        $repository = $this->getRepository($reportNameCollection);
        $excelReader = $this->getExcelReader($filePath);
        $inputFormatter = $this->getInputFormatter($versionParse);

        return new Report($repository, $excelReader, $versionParse, $inputFormatter);
    }

    /**
     * Devuelve una instancia de ExcelReader
     *
     * @param string $filePath Ruta del archivo
     *
     * @return null|LaravelExcelReader
     */
    protected function getExcelReader($filePath)
    {
        if (is_null($filePath)) {
            return null;
        }

        set_time_limit(1000);

        // configuración del encabezado, define para que muestre con el nombre
        // original, es decir sin formatear.
        Config::set('excel.import.heading', 'original');

        return Excel::load($filePath, function($reader) {
            // cachea 60 min el archivo subido.
            $reader->remember(60);
        });
    }

    /**
     * Devuelve una instancia de Repository
     *
     * @param string $reportNameCollection Nombre de la colección.
     *
     * @return Repository
     */
    protected function getRepository($reportNameCollection)
    {
        return new Repository($reportNameCollection);
    }

    protected function getInputFormatter($version)
    {
        if ($version === self::VERSION_STANDARD) {
            return new StandardInputFormatter();
        } else {
            return new RowExtraInputFormatter();
        }
    }
}