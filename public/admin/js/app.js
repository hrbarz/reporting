$('.table-click').on("click", "tr", function(e) {

	if ($(e.target).is("a,input")){ return true; }

	route = $(this).attr('href');
	if( !route ){ return false; }

	window.location = route;

});

$('.btn-filter').on("click", function(e) {

	$('.well-filters').toggle();

});

$('.modal-delete').on("click", function(e) {

	$('.modal-delete-type').html( $(this).data('type') );
	$('.modal-delete-item').html( $(this).data('item') );
	$('.btn-cta').attr('href', $(this).data('target') );
	$('#deleteModal').modal();

	return false;

});


/* UPLOAD PHOTOS */
$('.btn-upload input').on("change", function(e){ 

	files = e.target.files

	if( files.length > 0 )
	{
		$('.upload-caption').html( files.length + " files" );
		$('.upload-caption').removeClass('hidden');
	}
	else
	{
		$('.upload-caption').addClass('hidden');
	}

});

$('.btn-photo-remove').on("click", function(e){ 

	photo_id = $(this).data('id');
	$('#photo_' + photo_id).remove();

});

$('.btn-media-remove').on("click", function(e){ 

	media_id = $(this).data('id');
	$('#media_' + media_id).remove();
	$('#media_delete_' + media_id).val( media_id );

});

$('.btn-menu-left').on("click", function(e){ 

	if($(this).find('.fa-chevron-right').length > 0)
	{		
		$('#sidebar-wrapper').addClass('view-menu-left');
		$(this).find('.fa').removeClass('fa-chevron-right');
		$(this).find('.fa').addClass('fa-chevron-left');

	}else
	{
		$('#sidebar-wrapper').removeClass('view-menu-left');
		$(this).find('.fa').addClass('fa-chevron-right');
		$(this).find('.fa').removeClass('fa-chevron-left');
	}

});


$(function() {

	if($('#sidebar-wrapper').length == 0 )
	{
		$('.btn-menu-left').hide();
	}


	 $('textarea').not( '.tinymce' ).autoResize();
	 alertContainer();

});



function alertContainer()
{
	if( ('#wrapper').length )
	{
		$('.alert-container').prependTo('#wrapper');
	}
}
