<?php
	return [

		'columns' => [
			'radio' => 'MULTICHECK',
			'revista' => 'VALUE',
			'television' => 'MULTICHECK',
			'diario' => 'MULTICHECK',
			'internet' => 'VALUE',
			'redessociales' => 'CHECK'
		],

		'columns_v2' => ['radio_multi' =>'GROUPMULTICHECK_17',
										'canal_multi'=>'GROUPMULTICHECK_17',
										'revista'=>'VALUE',
										'diario' =>'MULTICHECK',
										'internet_multi'=>'GROUPMULTICHECK_2',
										'redessociales_num'=>'VALUE'],

		'columns_all' => [
			'radio' => 'MULTICHECK',
			'revista' => 'VALUE',
			'television' => 'MULTICHECK',
			'diario' => 'MULTICHECK',
			'internet' => 'VALUE',
			'redessociales' => 'CHECK',
			'radio_multi' =>'GROUPMULTICHECK_17',
			'canal_multi'=>'GROUPMULTICHECK_17',
			'revista'=>'VALUE',
			'diario' =>'MULTICHECK',
			'internet_multi'=>'GROUPMULTICHECK_2',
			'redessociales_num'=>'VALUE'
		],

		'medio' => ['radio', 'television', 'diario', 'internet', 'revista', 'redessociales'],

		'medio_2' => ['radio_multi'=>'Radio', 'canal_multi'=>'Televisión', 'diario'=>'Diarios', 'internet_multi'=>'Internet', 'revista'=>'Revistas', 'redessociales_num'=>'Redes Sociales'],

		'groups_default' => '[{"name":"radio","type":"MULTICHECK","from":"14","to":"29"},{"name":"radio","type":"MULTICHECK","from":"30","to":"45"},{"name":"radio","type":"MULTICHECK","from":"46","to":"61"},{"name":"radio","type":"MULTICHECK","from":"62","to":"77"},{"name":"radio","type":"MULTICHECK","from":"78","to":"93"},{"name":"radio","type":"MULTICHECK","from":"94","to":"109"},{"name":"radio","type":"MULTICHECK","from":"110","to":"125"},{"name":"radio","type":"MULTICHECK","from":"126","to":"141"},{"name":"radio","type":"MULTICHECK","from":"142","to":"157"},{"name":"radio","type":"MULTICHECK","from":"158","to":"173"},{"name":"radio","type":"MULTICHECK","from":"174","to":"189"},{"name":"radio","type":"MULTICHECK","from":"190","to":"205"},{"name":"radio","type":"MULTICHECK","from":"206","to":"221"},{"name":"radio","type":"MULTICHECK","from":"222","to":"237"},{"name":"radio","type":"MULTICHECK","from":"238","to":"253"},{"name":"radio","type":"MULTICHECK","from":"254","to":"269"},{"name":"radio","type":"MULTICHECK","from":"270","to":"285"},{"name":"radio","type":"MULTICHECK","from":"286","to":"301"},{"name":"radio","type":"MULTICHECK","from":"302","to":"317"},{"name":"radio","type":"MULTICHECK","from":"318","to":"333"},{"name":"radio","type":"MULTICHECK","from":"334","to":"349"},{"name":"radio","type":"MULTICHECK","from":"350","to":"365"},{"name":"radio","type":"MULTICHECK","from":"366","to":"381"},{"name":"radio","type":"MULTICHECK","from":"382","to":"397"},{"name":"radio","type":"MULTICHECK","from":"398","to":"413"},{"name":"radio","type":"MULTICHECK","from":"414","to":"429"},{"name":"radio","type":"MULTICHECK","from":"430","to":"445"},{"name":"radio","type":"MULTICHECK","from":"446","to":"461"},{"name":"radio","type":"MULTICHECK","from":"462","to":"477"},{"name":"radio","type":"MULTICHECK","from":"478","to":"493"},{"name":"radio","type":"MULTICHECK","from":"494","to":"509"},{"name":"radio","type":"MULTICHECK","from":"510","to":"525"},{"name":"radio","type":"MULTICHECK","from":"526","to":"541"},{"name":"radio","type":"MULTICHECK","from":"542","to":"557"},{"name":"radio","type":"MULTICHECK","from":"558","to":"573"},{"name":"radio","type":"MULTICHECK","from":"574","to":"589"},{"name":"radio","type":"MULTICHECK","from":"590","to":"605"},{"name":"radio","type":"MULTICHECK","from":"606","to":"621"},{"name":"radio","type":"MULTICHECK","from":"622","to":"637"},{"name":"radio","type":"MULTICHECK","from":"638","to":"653"},{"name":"radio","type":"MULTICHECK","from":"654","to":"669"},{"name":"radio","type":"MULTICHECK","from":"670","to":"685"},{"name":"radio","type":"MULTICHECK","from":"686","to":"701"},{"name":"radio","type":"MULTICHECK","from":"702","to":"717"},{"name":"radio","type":"MULTICHECK","from":"718","to":"733"},{"name":"radio","type":"MULTICHECK","from":"734","to":"749"},{"name":"radio","type":"MULTICHECK","from":"750","to":"765"},{"name":"radio","type":"MULTICHECK","from":"766","to":"781"},{"name":"radio","type":"MULTICHECK","from":"782","to":"797"},{"name":"radio","type":"MULTICHECK","from":"798","to":"813"},{"name":"radio","type":"MULTICHECK","from":"814","to":"829"},{"name":"radio","type":"MULTICHECK","from":"830","to":"845"},{"name":"radio","type":"MULTICHECK","from":"846","to":"861"},{"name":"radio","type":"MULTICHECK","from":"862","to":"877"},{"name":"radio","type":"MULTICHECK","from":"878","to":"893"},{"name":"radio","type":"MULTICHECK","from":"894","to":"925"},{"name":"radio","type":"MULTICHECK","from":"926","to":"941"},{"name":"radio","type":"MULTICHECK","from":"942","to":"957"},{"name":"radio","type":"MULTICHECK","from":"958","to":"973"},{"name":"radio","type":"MULTICHECK","from":"974","to":"989"},{"name":"radio","type":"MULTICHECK","from":"990","to":"1005"},{"name":"radio","type":"MULTICHECK","from":"1006","to":"1021"},{"name":"radio","type":"MULTICHECK","from":"1022","to":"1037"},{"name":"radio","type":"MULTICHECK","from":"1038","to":"1053"},{"name":"radio","type":"MULTICHECK","from":"1054","to":"1069"},{"name":"radio","type":"MULTICHECK","from":"1070","to":"1085"},{"name":"radio","type":"MULTICHECK","from":"1086","to":"1101"},{"name":"radio","type":"MULTICHECK","from":"1102","to":"1117"},{"name":"radio","type":"MULTICHECK","from":"1118","to":"1133"},{"name":"radio","type":"MULTICHECK","from":"1134","to":"1149"},{"name":"radio","type":"MULTICHECK","from":"1150","to":"1165"},{"name":"radio","type":"MULTICHECK","from":"1166","to":"1181"},{"name":"radio","type":"MULTICHECK","from":"1182","to":"1197"},{"name":"radio","type":"MULTICHECK","from":"1198","to":"1213"},{"name":"diario","type":"MULTICHECK","from":"1214","to":"1220"},{"name":"diario","type":"MULTICHECK","from":"1221","to":"1227"},{"name":"diario","type":"MULTICHECK","from":"1228","to":"1234"},{"name":"diario","type":"MULTICHECK","from":"1235","to":"1241"},{"name":"diario","type":"MULTICHECK","from":"1242","to":"1248"},{"name":"diario","type":"MULTICHECK","from":"1249","to":"1255"},{"name":"diario","type":"MULTICHECK","from":"1256","to":"1262"},{"name":"revista","type":"VALUE","from":"1263","to":"1271"},{"name":"television","type":"MULTICHECK","from":"1280","to":"1295"},{"name":"television","type":"MULTICHECK","from":"1296","to":"1311"},{"name":"television","type":"MULTICHECK","from":"1312","to":"1327"},{"name":"television","type":"MULTICHECK","from":"1328","to":"1343"},{"name":"television","type":"MULTICHECK","from":"1344","to":"1359"},{"name":"television","type":"MULTICHECK","from":"1360","to":"1375"},{"name":"television","type":"MULTICHECK","from":"1376","to":"1391"},{"name":"television","type":"MULTICHECK","from":"1392","to":"1407"},{"name":"television","type":"MULTICHECK","from":"1408","to":"1423"},{"name":"television","type":"MULTICHECK","from":"1424","to":"1439"},{"name":"television","type":"MULTICHECK","from":"1440","to":"1455"},{"name":"television","type":"MULTICHECK","from":"1456","to":"1471"},{"name":"television","type":"MULTICHECK","from":"1472","to":"1487"},{"name":"television","type":"MULTICHECK","from":"1488","to":"1503"},{"name":"television","type":"MULTICHECK","from":"1504","to":"1519"},{"name":"television","type":"MULTICHECK","from":"1520","to":"1535"},{"name":"television","type":"MULTICHECK","from":"1536","to":"1551"},{"name":"internet","type":"VALUE","from":"1552","to":"1578"},{"name":"redessociales","type":"CHECK","from":"1579","to":"1596"}]',

		'view' => [
			'columns' => [
				'radio' => ['6a7' => '6 a 7', '7a8' => '7 a 8','8a9' => '8 a 9','9a10' => '9 a 10','10a11' => '10 a 11','11a12' => '11 a 12','12a13' => '12 a 13','13a14' => '13 a 14','14a15' => '14 a 15','15a16' => '15 a 16','16a17' => '16 a 17','17a18' => '17 a 18','18a19' => '18 a 19','19a20' => '19 a 20','20a21' => '20 a 21','21a6' => '21 a 6'
				],

				'radio_multi' => ['6' => '6 hs', '7' => '7 hs','8' => '8 hs','9' => '9 hs','10' => '10 hs','11' => '11 hs','12' => '12 hs','13' => '13 hs','14' => '14 hs','15' => '15 hs','16' => '16 hs','17' => '17 hs','18' => '18 hs','19' => '19 hs','20' => '20 hs','21a6' => '21 hs a 6 hs'
				],

				'television' => ['6a9' => '6 a 9','8a9' => '8 a 9','9a10' => '9 a 10','10a11' => '10 a 11','11a12' => '11 a 12','12a13' => '12 a 13','13a14' => '13 a 14','14a15' => '14 a 15','15a16' => '15 a 16','16a17' => '16 a 17','17a18' => '17 a 18','18a19' => '18 a 19','19a20' => '19 a 20','20a21' => '20 a 21','21a22' => '21 a 22','22a23' => '22 a 23'
				],

				'canal_multi' => ['6a9' => '6hs a 9hs','10' => '10 hs','11' => '11hs','12' => '12 hs','13' => '13 hs','14' => '14 hs','15' => '15 hs','16' => '16 hs','17' => '17 hs','18' => '18 hs','19' => '19 hs','20' => '20 hs','21' => '21 hs','22' => '22 hs','23a6' => '23 hs a 6 hs',
				],


				'diario' => [
					'Lunes' => 'Lunes', 'Martes' => 'Martes', 'Miércoles' => 'Miércoles', 'Jueves' => 'Jueves', 'Viernes' => 'Viernes', 'Sábado' => 'Sábado', 'Domingo' => 'Domingo'
				],

				'internet' => [
					'1 vez por día' => '1 vez por día', 
					'Varias veces por día' => 'Varias veces por día', 
					'Menos de 1 vez por semana' => 'Menos de 1 vez por semana',  
					'1 vez por semana' => '1 vez por semana',
					'Varias veces por semana' => 'Varias veces por semana'
				],

				'internet_multi' => [
					'1 vez por día' => '1 vez por día', 
					'Varias veces por día' => 'Varias veces por día', 
					'Menos de 1 vez por semana' => 'Menos de 1 vez por semana',  
					'1 vez por semana' => '1 vez por semana',
					'Varias veces por semana' => 'Varias veces por semana',
				],

				'revista' => [
					'Siempre' => 'Siempre', 'Casi siempre' => 'Casi siempre','A veces' => 'A veces'
				],
				
			]
		],

		'factor' => [
		
			'diario' => [
				'Comodoro Rivadavia' => 62,
				'Trelew'	=> 87,
				'Puerto Madryn' => 61,
				'Esquel' => 31
			],
			'radio' => [
				'Comodoro Rivadavia' => 230,
				'Trelew'	=> 362,
				'Puerto Madryn' => 260,
				'Esquel' => 115
			],
			'television' => [
				'Comodoro Rivadavia' => 326,
				'Trelew'	=> 475,
				'Puerto Madryn' => 340,
				'Esquel' => 168
			],
			'internet' => [
				'Comodoro Rivadavia' => 610,
				'Trelew'	=> 1646,
				'Puerto Madryn' => 1054,
				'Esquel' => 557
			],
			'revista' => [
				'Comodoro Rivadavia' => 126,
				'Trelew'	=> 943,
				'Puerto Madryn' => 418,
				'Esquel' => 333
			],
			'redessociales' => [
				'Comodoro Rivadavia' => 329,
				'Trelew'	=> 462,
				'Puerto Madryn' => 326,
				'Esquel' => 170
			],
		],

        'name_exceptions' => ['Ninguna de las anteriores','Ninguno de los anteriores','Ninguna otra', 'No escucho otra radio']
	];