<?php

return [

	'app' => [
		'name' => 'Reporting',
		'path_upload' => '/upload',
		'files_cloud' => 'https://s3.amazonaws.com/files.3comunicaciones.com.ar'
	],

	'status' => [
		'general' => ['active','inactive']
	],

	'type' =>  ['general','medios','particular']

];