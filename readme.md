# Reporte de 3Com

## Instalación

Si se está configurando por primera vez copiar el .env
> cp .env.example .env

Para el contenedor de la aplicacion
> docker-compose up -d

Ingresar al contenedor
> docker exec -ti reporting bash

Dentro del contenedor instalar las dependecias

> cd packages/component/report

> composer update

> cd /var/www/app/

> composer update